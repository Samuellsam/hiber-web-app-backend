﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokoBangunanHiber.Entities.Enums
{
    public enum PaymentTypeEnum
    {
        [Display(Name = "Kredit")]
        Kredit = 1,

        [Display(Name = "Tunai")]
        Tunai
    }
}
