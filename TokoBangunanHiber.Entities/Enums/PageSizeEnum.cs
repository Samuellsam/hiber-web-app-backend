﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokoBangunanHiber.Entities.Enums
{
    public enum PageSizeEnum
    {
        [Display(Name = "A0")]
        A0 = 1,

        [Display(Name = "A1")]
        A1,

        [Display(Name = "A2")]
        A2,

        [Display(Name = "A3")]
        A3,

        [Display(Name = "A4")]
        A4,

        [Display(Name = "A6")]
        A6,

        [Display(Name = "A7")]
        A7,

        [Display(Name = "A8")]
        A8,

        [Display(Name = "A9")]
        A9,

        [Display(Name = "A10")]
        A10,
    }
}
