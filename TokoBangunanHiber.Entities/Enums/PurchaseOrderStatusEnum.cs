﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TokoBangunanHiber.Entities.Enums
{
    public enum PurchaseOrderStatusEnum
    {
        [Display(Name = "Waiting for Approval")]
        WaitingForApproval = 1,

        [Display(Name = "Declined")]
        Declined = 2,

        [Display(Name = "Approved")]
        Approved = 3,

        [Display(Name = "Pengajuan Retur")]
        PengajuanRetur = 4,

        [Display(Name = "Selesai")]
        Selesai = 5
    }
}
