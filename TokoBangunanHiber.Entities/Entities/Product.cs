﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class Product
    {
        public Product()
        {
            PurchaseOrderDetails = new HashSet<PurchaseOrderDetail>();
            ReturProducts = new HashSet<ReturProduct>();
            TransactionDetails = new HashSet<TransactionDetail>();
        }

        public Guid ProductId { get; set; }
        public Guid? BlobId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public decimal BasePrice { get; set; }
        public int Stock { get; set; }
        public Guid SupplierId { get; set; }
        public int UnitId { get; set; }
        public int CategoryId { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string BarcodeId { get; set; }
        public string ImageName { get; set; }
        public int? MinimumStockLimit { get; set; }

        public virtual Blob Blob { get; set; }
        public virtual Category Category { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual Unit Unit { get; set; }
        public virtual ICollection<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
        public virtual ICollection<ReturProduct> ReturProducts { get; set; }
        public virtual ICollection<TransactionDetail> TransactionDetails { get; set; }
    }
}
