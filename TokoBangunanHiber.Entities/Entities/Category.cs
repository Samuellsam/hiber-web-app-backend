﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class Category
    {
        public Category()
        {
            Products = new HashSet<Product>();
            Units = new HashSet<Unit>();
        }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
    }
}
