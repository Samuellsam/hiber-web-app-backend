﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class Transaction
    {
        public Transaction()
        {
            Deliveries = new HashSet<Delivery>();
            PaymentHistories = new HashSet<PaymentHistory>();
            ReturProducts = new HashSet<ReturProduct>();
            TransactionDetails = new HashSet<TransactionDetail>();
        }

        public Guid TransactionId { get; set; }
        public int? PaymentTypeId { get; set; }
        public decimal TotalPrice { get; set; }
        public string BuyerName { get; set; }
        public DateTime? DeadlineDate { get; set; }
        public decimal? Interest { get; set; }
        public bool IsPaid { get; set; }
        public DateTime CreatedAt { get; set; }

        public virtual PaymentType PaymentType { get; set; }
        public virtual ICollection<Delivery> Deliveries { get; set; }
        public virtual ICollection<PaymentHistory> PaymentHistories { get; set; }
        public virtual ICollection<ReturProduct> ReturProducts { get; set; }
        public virtual ICollection<TransactionDetail> TransactionDetails { get; set; }
    }
}
