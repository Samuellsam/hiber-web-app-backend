﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class PaymentType
    {
        public PaymentType()
        {
            Transactions = new HashSet<Transaction>();
        }

        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
