﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class Blob
    {
        public Blob()
        {
            Products = new HashSet<Product>();
        }

        public Guid BlobId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string MimeType { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
