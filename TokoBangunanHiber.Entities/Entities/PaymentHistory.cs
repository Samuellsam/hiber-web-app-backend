﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class PaymentHistory
    {
        public Guid PaymentHistoryId { get; set; }
        public Guid TransactionId { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Payment { get; set; }

        public virtual Transaction Transaction { get; set; }
    }
}
