﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class ReturProduct
    {
        public Guid ReturProductId { get; set; }
        public Guid TransactionId { get; set; }
        public Guid ProductId { get; set; }
        public Guid? DeliveryId { get; set; }
        public int Quantity { get; set; }
        public DateTime CreatedAt { get; set; }

        public virtual Delivery Delivery { get; set; }
        public virtual Product Product { get; set; }
        public virtual Transaction Transaction { get; set; }
    }
}
