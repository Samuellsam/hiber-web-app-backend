﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TokoBangunanHiber.Entities.Entities
{
    public partial class DeliveryStatus
    {
        public DeliveryStatus()
        {
            Deliveries = new HashSet<Delivery>();
        }

        public int DeliveryStatusId { get; set; }
        public string DeliveryStatusName { get; set; }

        public virtual ICollection<Delivery> Deliveries { get; set; }
    }
}
