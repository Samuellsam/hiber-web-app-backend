CREATE TABLE purchase_order_status
(
	purchase_order_status_id INT GENERATED ALWAYS AS IDENTITY
        CONSTRAINT purchase_order_status_pk PRIMARY KEY,
	purchase_order_status_name VARCHAR(255) NOT NULL
);

CREATE TABLE "role"
(
	role_id INT GENERATED ALWAYS AS IDENTITY
        CONSTRAINT role_pk PRIMARY KEY,
	role_name VARCHAR(255) NOT NULL
);

CREATE TABLE category
(
	category_id INT GENERATED ALWAYS AS IDENTITY
        CONSTRAINT category_pk PRIMARY KEY,
	category_name VARCHAR(255) NOT NULL
);

CREATE TABLE payment_type
(
	payment_type_id INT GENERATED ALWAYS AS IDENTITY
		CONSTRAINT payment_type_pk PRIMARY KEY,
	payment_type_name VARCHAR(255) NOT NULL
);

CREATE TABLE "blob"
(
	blob_id UUID
		CONSTRAINT blob_pk PRIMARY KEY,
	file_name TEXT NOT NULL,
    	file_path TEXT NOT NULL,
    	mime_type TEXT NOT NULL
);

CREATE TABLE unit
(
	unit_id INT GENERATED ALWAYS AS IDENTITY
        CONSTRAINT unit_pk PRIMARY KEY,
category_id INT NOT NULL
        CONSTRAINT un__category_fk REFERENCES category,
	unit_name VARCHAR(255) NOT NULL
);

CREATE TABLE delivery_status
(
	delivery_status_id INT GENERATED ALWAYS AS IDENTITY
        CONSTRAINT delivery_status_pk PRIMARY KEY,
	delivery_status_name VARCHAR(255) NOT NULL
);

CREATE TABLE supplier
(
	supplier_id UUID NOT NULL
		CONSTRAINT supplier_pk PRIMARY KEY,
	supplier_name VARCHAR(255) NOT NULL,
	supplier_email VARCHAR(255) NOT NULL,
	supplier_phone VARCHAR(255) NOT NULL
);

CREATE TABLE "user"
(	
	user_id UUID NOT NULL
		CONSTRAINT user_pk PRIMARY KEY,
	“username” VARCHAR(255) NOT NULL,
	“password” VARCHAR(255) NOT NULL,
	“name” VARCHAR(255) NOT NULL,
	“email” VARCHAR(255) NOT NULL,
	role_id INT NOT NULL
		CONSTRAINT u__role_fk REFERENCES "role"
);

CREATE TABLE "transaction"
(
	transaction_id UUID
		CONSTRAINT transaction_pk PRIMARY KEY,
	payment_type_id INT
		CONSTRAINT tr__payment_type_fk REFERENCES payment_type,
	total_price DECIMAL NOT NULL,
	buyer_name VARCHAR(255) NOT NULL,
	deadline_date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
	interest DECIMAL NOT NULL,
	is_paid BOOLEAN NOT NULL,
	created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE product
(
	product_id UUID NOT NULL
		CONSTRAINT product_pk PRIMARY KEY,
	blob_id UUID
CONSTRAINT p_blob_fk REFERENCES "blob",
	product_name VARCHAR(255) NOT NULL,
	price DECIMAL NOT NULL,
	base_price DECIMAL NOT NULL,
	stock INT NOT NULL,
	supplier_id UUID NOT NULL
CONSTRAINT p__supplier_fk REFERENCES supplier,
	unit_id INT NOT NULL
CONSTRAINT p__unit_fk REFERENCES unit,
	category_id INT NOT NULL
CONSTRAINT p__category_fk REFERENCES category,
	updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE transaction_detail
(
	transaction_detail_id UUID
		CONSTRAINT transaction_detail_pk PRIMARY KEY,
	transaction_id UUID
		CONSTRAINT td__transaction_fk REFERENCES "transaction",
	product_id UUID
		CONSTRAINT td__product_fk REFERENCES product,
	quantity INT NOT NULL,
	price DECIMAL NOT NULL
);

CREATE TABLE payment_history
(
	payment_history_id UUID
		CONSTRAINT payment_history_pk PRIMARY KEY,
	transaction_id UUID NOT NULL
		CONSTRAINT ph__transaction REFERENCES "transaction",
	payment_date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
	payment DECIMAL NOT NULL
);

CREATE TABLE delivery
(
	delivery_id UUID NOT NULL
		CONSTRAINT delivery_pk PRIMARY KEY,
	transaction_id UUID NOT NULL
		CONSTRAINT d__transaction_fk REFERENCES "transaction",
	user_id UUID NOT NULL
		CONSTRAINT d__user_fk REFERENCES "user",
	delivery_status_id INT NOT NULL
		CONSTRAINT d__delivery_status_fk REFERENCES delivery_status
);

CREATE TABLE purchase_order
(
	purchase_order_id  UUID NOT NULL
CONSTRAINT purchase_order_pk PRIMARY KEY,
total_price DECIMAL NOT NULL,
created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
approved_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
supplier_id UUID NOT NULL 
	CONSTRAINT po__supplier_fk REFERENCES supplier,
purchase_order_status_id INT NOT NULL
	CONSTRAINT po__purchase_order_status_fk REFERENCES purchase_order_status
);

CREATE TABLE purchase_order_detail 
(
	purchase_order_detail_id UUID NOT NULL
		CONSTRAINT purchase_order_detail_pk PRIMARY KEY,
	quantity INT NOT NULL,
	price DECIMAL NOT NULL,
	product_id UUID NOT NULL
		CONSTRAINT pod__product_fk REFERENCES product,
	purchase_order_id UUID
		CONSTRAINT pod_purchase_order_fk REFERENCES purchase_order
);

-- Purchase order status
INSERT INTO purchase_order_status(purchase_order_status_name) VALUES ('Waiting for Approval');
INSERT INTO purchase_order_status(purchase_order_status_name) VALUES ('Declined');
INSERT INTO purchase_order_status(purchase_order_status_name) VALUES ('Approved');

-- Role
INSERT INTO "role"(role_name) VALUES ('Admin');
INSERT INTO "role"(role_name) VALUES ('Employee');

--Category
INSERT INTO category(category_name) VALUES ('Cat');
INSERT INTO category(category_name) VALUES ('Batako');
INSERT INTO category(category_name) VALUES ('Hebel');
INSERT INTO category(category_name) VALUES ('Pasir');
INSERT INTO category(category_name) VALUES ('Paku');
INSERT INTO category(category_name) VALUES ('Kayu');
INSERT INTO category(category_name) VALUES ('Baja ringan');
INSERT INTO category(category_name) VALUES ('Alat Elektronik');
INSERT INTO category(category_name) VALUES ('Bahan Bangunan');
INSERT INTO category(category_name) VALUES ('Tali');
INSERT INTO category(category_name) VALUES ('Perkakas Tukang');
INSERT INTO category(category_name) VALUES ('Semen');

--Payment Type
INSERT INTO payment_type(payment_type_name) VALUES('Kredit');
INSERT INTO payment_type(payment_type_name) VALUES('Tunai');

-- Delivery status
INSERT INTO delivery_status(delivery_status_name) VALUES ('Memuat barang');
INSERT INTO delivery_status(delivery_status_name) VALUES ('Menuju Customer');
INSERT INTO delivery_status(delivery_status_name) VALUES ('Barang sampai');
