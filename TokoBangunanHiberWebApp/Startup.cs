using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Extentions;
using TokoBangunanHiberWebApp.Service;

namespace TokoBangunanHiberWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddAuthService();
            services.AddBCryptService();
            services.AddUserService();
            services.AddTransactionService();
            services.AddPaymentTypeService();
            services.AddReturProductService();
            services.AddDeliveryService();
            services.AddCashierService();
            services.AddPdfService();
            services.AddProductService();
            services.AddSupplierService();
            services.AddPurchaseOrderService();
            services.AddUnitService();
            services.AddCategoryService();
            services.AddDashboardService();
            services.AddRoleService();
            services.AddDbContextPool<HiberDBContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("HiberDB"));
            });
            services.AddSwaggerDocument();
            services.AddCors(option =>
            {
                option.AddPolicy("HiberNextFrontEnd", policy =>
                {
                    policy.WithOrigins("http://localhost:3000")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath,"Images/Products")),
                RequestPath= "/Images/Products"
            });

            app.UseSwaggerUi3();

            app.UseRouting();

            app.UseCors("HiberNextFrontEnd");

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
