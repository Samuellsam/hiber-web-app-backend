﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class BCryptHasher:IBCryptHasher
    {
        public string HashString(string text)
        {
            return BCrypt.Net.BCrypt.HashPassword(text);
        }

        public bool Verify(string text, string hashedString)
        {
            var isValid = BCrypt.Net.BCrypt.Verify(text, hashedString);

            return isValid;
        }
    }
}
