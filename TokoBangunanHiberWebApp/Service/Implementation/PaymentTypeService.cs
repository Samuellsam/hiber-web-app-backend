﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model.PaymentType;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class PaymentTypeService : IPaymentTypeService
    {
        public PaymentTypeService(HiberDBContext db)
        {
            this.DB = db;
        }
        private readonly HiberDBContext DB;

        public async Task<List<PaymentTypeViewModel>> GetAllPaymentType()
        {
            var paymentTypes = await DB.PaymentTypes
                                .AsNoTracking()
                                .Select(Q => new PaymentTypeViewModel
                                {
                                    PaymentTypeId = Q.PaymentTypeId,
                                    PaymentTypeName = Q.PaymentTypeName
                                }).ToListAsync();

            return paymentTypes;
        }
    }
}
