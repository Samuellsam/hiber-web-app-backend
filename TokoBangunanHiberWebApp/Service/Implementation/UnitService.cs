﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model.Category;
using TokoBangunanHiberWebApp.Model.Unit;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class UnitService : IUnitService
    {
        public UnitService(HiberDBContext db)
        {
            this.DB = db;
        }
        private readonly HiberDBContext DB;

        public async Task<bool> CreateUnit(UnitCreateUpdateModel form)
        {
            var category = await DB.Units
                .Where(Q => Q.CategoryId == form.CategoryId)
                .FirstOrDefaultAsync();

            if(category != null)
            {
                return false;
            }

            var newUnit = new Unit
            {
                CategoryId = form.CategoryId,
                UnitName = form.UnitName
            };

            DB.Units.Add(newUnit);
            await DB.SaveChangesAsync();

            return true;
        }

        public async Task<UnitViewModel> GetUnit(int unitId)
        {
            var unit = await DB.Units.AsNoTracking()
                .Where(Q => Q.UnitId == unitId)
                .Select(Q => new UnitViewModel
                {
                    UnitName = Q.UnitName,
                    UnitId = Q.UnitId,
                    CategoryId = Q.CategoryId,
                    CategoryName = (from c in DB.Categories
                                    where c.CategoryId == Q.CategoryId
                                    select c.CategoryName).FirstOrDefault()
                }).FirstOrDefaultAsync();

            return unit;
        }

        public async Task<List<CategoryViewModel>> GetRemainingCategory()
        {
            var existingCategories = await DB.Units
                                           .Select(Q => Q.CategoryId)
                                           .Distinct()
                                           .ToListAsync();
            var data = await DB.Categories
                            .Where(Q => !existingCategories.Contains(Q.CategoryId))
                            .Select(Q => new CategoryViewModel
                            {
                                CategoryId = Q.CategoryId,
                                CategoryName = Q.CategoryName
                            })
                            .ToListAsync();
            return data;
        }

        public async Task<UnitPaginationModel> GetAllUnit(FilterUnitListModel filter)
        {
            var dataUnits = new UnitPaginationModel();
            var data = await DB.Units.AsNoTracking()
                .OrderBy(Q => Q.UnitId)
                .Select(Q => new UnitViewModel
                {
                    UnitName = Q.UnitName,
                    UnitId = Q.UnitId,
                    CategoryName = (from c in DB.Categories
                                  where c.CategoryId == Q.CategoryId
                                  select c.CategoryName).FirstOrDefault()
                }).AsNoTracking().ToListAsync();

            if (!string.IsNullOrEmpty(filter.UnitName))
            {
                data = data.Where(Q => Q.UnitName.ToLower().Contains(filter.UnitName.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(filter.CategoryName))
            {
                data = data.Where(Q => Q.CategoryName.ToLower().Contains(filter.CategoryName.ToLower())).ToList();
            }

            dataUnits.TotalData = data.Count();

            if (filter.PageIndex != 0 && filter.ItemPerPage != 0)
            {
                data = data.Skip((filter.PageIndex - 1) * filter.ItemPerPage).Take(filter.ItemPerPage).ToList();
            }

            dataUnits.Units = data;

            return dataUnits;
        }

        public async Task<UnitCreateUpdateResponseModel> UpdateUnit(UnitCreateUpdateModel form, int unitId)
        {
            var unit = await DB.Units
                .Where(Q => Q.UnitId == unitId)
                .FirstOrDefaultAsync();

            var category = await DB.Categories
                .Where(Q => Q.CategoryId == form.CategoryId)
                .FirstOrDefaultAsync();

            var unitWithSameCategoryCount = await DB.Units
                .Where(Q => Q.CategoryId == form.CategoryId && Q.UnitId != unitId)
                .CountAsync();

            if(string.IsNullOrEmpty(form.UnitName))
            {
                return new UnitCreateUpdateResponseModel
                {
                    ErrorMessage = "Nama unit tidak boleh kosong",
                    IsSuccess = false
                };
            }

            if (unit == null)
            {
                return new UnitCreateUpdateResponseModel
                {
                    ErrorMessage = "Unit tidak terdapat pada database",
                    IsSuccess = false
                };
            }

            if (category == null)
            {
                return new UnitCreateUpdateResponseModel
                {
                    ErrorMessage = "Category tidak terdapat pada database",
                    IsSuccess = false
                };
            }

            if (category == null)
            {
                return new UnitCreateUpdateResponseModel
                {
                    ErrorMessage = "Category tidak terdapat pada database",
                    IsSuccess = false
                };
            }

            if (unitWithSameCategoryCount > 0)
            {
                return new UnitCreateUpdateResponseModel
                {
                    ErrorMessage = "Category sudah memiliki unit",
                    IsSuccess = false
                };
            }

            unit.CategoryId = form.CategoryId;
            unit.UnitName = form.UnitName;

            await DB.SaveChangesAsync();

            return new UnitCreateUpdateResponseModel
            {
                ErrorMessage = null,
                IsSuccess = true
            };
        }
    }
}
