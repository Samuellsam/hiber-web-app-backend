﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model.PaymentHistory;
using TokoBangunanHiberWebApp.Model.ReturProduct;
using TokoBangunanHiberWebApp.Model.Transaction;
using TokoBangunanHiberWebApp.Service.Interface;
using TokoBangunanHiber.Entities.Enums;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class TransactionService : ITransactionService
    {
        public TransactionService(HiberDBContext db)
        {
            this.DB = db;
        }
        private readonly HiberDBContext DB;

        public async Task<TransactionPaginationModel> GetAllTransaction(TransactionFilterModel filter)
        {
            var dataTransactions = new TransactionPaginationModel();

            var data =  await (from t in DB.Transactions
                        join pt in DB.PaymentTypes on t.PaymentTypeId equals pt.PaymentTypeId
                        orderby t.CreatedAt descending
                        select new TransactionViewModel
                        {
                            BuyerName = t.BuyerName,
                            IsPaid = t.IsPaid,
                            PaymentTypeId = pt.PaymentTypeId,
                            PaymentTypeName = pt.PaymentTypeName,
                            TotalPrice = t.TotalPrice,
                            TransactionId = t.TransactionId,
                            IsInReturProcess = (from rp in DB.ReturProducts
                                                join d in DB.Deliveries on rp.DeliveryId equals d.DeliveryId
                                                where rp.TransactionId == t.TransactionId && d.DeliveryStatusId != (int)DeliveryStatusEnum.BarangSampai
                                                select d.DeliveryStatusId).FirstOrDefault() == 0 ? false : true,
                            CreatedAt = t.CreatedAt
                        }).AsNoTracking().ToListAsync();

            if (filter.TransactionId != null)
            {
                data = data.Where(Q => Q.TransactionId.Equals(filter.TransactionId)).ToList();
            }
            if (!string.IsNullOrEmpty(filter.BuyerName))
            {
                data = data.Where(Q => Q.BuyerName.ToLower().Contains(filter.BuyerName.ToLower())).ToList();
            }
            if (filter.IsPaid != null)
            {
                data = data.Where(Q => Q.IsPaid == filter.IsPaid).ToList();
            }
            if (filter.PaymentTypeId != null)
            {
                data = data.Where(Q => Q.PaymentTypeId == filter.PaymentTypeId).ToList();
            }
            if (filter.DateFrom != null)
            {
                data = data.Where(Q => Q.CreatedAt >= filter.DateFrom).ToList();
            }
            if (filter.DateTo != null)
            {
                data = data.Where(Q => Q.CreatedAt <= filter.DateTo).ToList();
            }

            dataTransactions.TotalData = data.Count();

            if (filter.PageIndex != 0 && filter.ItemPerPage != 0)
            {
                data = data.Skip((filter.PageIndex - 1) * filter.ItemPerPage).Take(filter.ItemPerPage).ToList();
            }

            dataTransactions.Transactions = data;

            return dataTransactions;

        }

        public async Task<TransactionDetailViewModel> GetTransactionByTransactionId(Guid transactionId)
        {
            var returProductDeliveryId = await DB.ReturProducts
                .Where(Q => Q.TransactionId.Equals(transactionId))
                .Select(Q => Q.DeliveryId)
                .ToListAsync();

            var deliveryData = await (from d in DB.Deliveries
                                      where d.TransactionId == transactionId && !returProductDeliveryId.Contains(d.DeliveryId)
                                      select d).FirstOrDefaultAsync();

            var listReturHistory = await (from rp in DB.ReturProducts
                                          join p in DB.Products on rp.ProductId equals p.ProductId
                                          where rp.TransactionId == transactionId
                                          orderby rp.CreatedAt descending
                                          select new ReturProductViewModel
                                          {
                                              TransactionId = transactionId,
                                              CreatedAt = rp.CreatedAt,
                                              DeliveryId = rp.DeliveryId,
                                              ProductName = p.ProductName,
                                              DeliveryStatusId = rp.DeliveryId == null ?
                                              (int)DeliveryStatusEnum.BarangSampai : (from d in DB.Deliveries
                                                                                      where d.DeliveryId == rp.DeliveryId
                                                                                      select d.DeliveryStatusId).FirstOrDefault(),
                                              ProductQuantity = rp.Quantity
                                          }).ToListAsync();

            var transaction = await (from t in DB.Transactions
                                  join pt in DB.PaymentTypes on t.PaymentTypeId equals pt.PaymentTypeId
                                  where t.TransactionId == transactionId
                                  select new TransactionDetailViewModel
                                  {
                                      DeliveryId = deliveryData != null ? deliveryData.DeliveryId : null,
                                      BuyerName = t.BuyerName,
                                      IsPaid = t.IsPaid,
                                      DeliveryStatusId = deliveryData != null ? deliveryData.DeliveryStatusId : null,
                                      ReturDeliveryStatusId = listReturHistory.Count > 0 ? listReturHistory.FirstOrDefault().DeliveryStatusId : null,
                                      PaymentTypeName = pt.PaymentTypeName,
                                      PaymentTypeId = pt.PaymentTypeId,
                                      TotalPrice = t.TotalPrice,
                                      TransactionDate = t.CreatedAt.ToString("dd/MM/yyyy"),
                                      Interest = t.Interest != null ? (decimal)t.Interest : 0,
                                      TotalPayment = (from ph in DB.PaymentHistories
                                                      where ph.TransactionId == t.TransactionId
                                                      select ph.Payment).Sum(),
                                      ListBoughtProduct = (from td in DB.TransactionDetails
                                                           join p in DB.Products on td.ProductId equals p.ProductId
                                                           where td.TransactionId == t.TransactionId
                                                           select new TransactionProductDetail
                                                           {
                                                               ProductId = p.ProductId,
                                                               ProductName = p.ProductName,
                                                               TotalPrice = td.Price,
                                                               Quantity = td.Quantity
                                                           }).ToList(),
                                      ListPaymentHistory = (from ph in DB.PaymentHistories
                                                            where ph.TransactionId == t.TransactionId
                                                            orderby ph.PaymentDate descending
                                                            select new PaymentHistoryViewModel
                                                            {
                                                                PaymentDate = ph.PaymentDate,
                                                                TotalPayment = ph.Payment
                                                            }).ToList(),
                                      ListReturHistory = listReturHistory
                                  }).FirstOrDefaultAsync();

            return transaction;
        }

        public async Task<bool> PayCreditTransaction(Guid transactionId, TransactionPayModel form)
        {
            var transaction = await DB.Transactions
                .Where(Q => Q.TransactionId == transactionId)
                .FirstOrDefaultAsync();

            var newPaymentHistory = new PaymentHistory
            {
                PaymentHistoryId = Guid.NewGuid(),
                PaymentDate = DateTime.UtcNow,
                TransactionId = transactionId,
                Payment = form.Payment
            };

            if(form.IsLastPayment)
            {
                transaction.IsPaid = true;
            }

            DB.PaymentHistories.Add(newPaymentHistory);
            await DB.SaveChangesAsync();

            return true;
        }
    }
}
