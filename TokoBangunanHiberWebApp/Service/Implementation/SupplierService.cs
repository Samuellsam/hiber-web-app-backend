﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model.Supplier;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class SupplierService : ISupplierService
    {
        public SupplierService(HiberDBContext db)
        {
            this.DB = db;
        }

        private readonly HiberDBContext DB;

        public async Task<List<SupplierViewModel>> GetAllAsync()
        {
            var suppliers = await this.DB
                .Suppliers
                .AsNoTracking()
                .Select(Q => new SupplierViewModel
                { 
                    SupplierId = Q.SupplierId,
                    Name = Q.SupplierName
                })
                .ToListAsync();

            return suppliers;
        }



        public async Task<bool> CreateSupplier(SupplierCreateEditModel model)
        {
            var newData = new Supplier
            {
                SupplierId = Guid.NewGuid(),
                SupplierEmail = model.SupplierEmail,
                SupplierName = model.SupplierName,
                SupplierPhone = model.SupplierPhone
            };

            DB.Suppliers.Add(newData);
            await DB.SaveChangesAsync();
            return true;
        }

        public async Task<bool> EditSupplier(SupplierCreateEditModel model, Guid supplierId)
        {
            var data = await this.DB.Suppliers
                             .Where(Q => Q.SupplierId == supplierId)
                             .FirstOrDefaultAsync();

            data.SupplierName = model.SupplierName;
            data.SupplierPhone = model.SupplierPhone;
            data.SupplierEmail = model.SupplierEmail;

            await DB.SaveChangesAsync();

            return true;

        }

        public async Task<SupplierDetailModel> GetSupplierDetail(Guid supplierId)
        {
            var data = await this.DB.Suppliers
                             .Where(Q => Q.SupplierId == supplierId)
                             .Select(Q => new SupplierDetailModel
                             {
                                 SupplierEmail = Q.SupplierEmail,
                                 SupplierName = Q.SupplierName,
                                 SupplierPhone = Q.SupplierPhone,
                                 SupplierId = Q.SupplierId
                             })
                             .FirstOrDefaultAsync();


            return data;
        }

        public async Task<bool> DeleteSupplier(Guid supplierId)
        {
            var data = await this.DB.Suppliers
                             .Where(Q => Q.SupplierId == supplierId)
                             .FirstOrDefaultAsync();

            if(data == null)
            {
                return false;
            }

            DB.Suppliers.Remove(data);
            await DB.SaveChangesAsync();
            return true;
        }

        public async Task<SupplierPaginationModel> GetSupplierList(FilterSupplierListModel filter)
        {
            var dataSupplier = new SupplierPaginationModel();
            var data = await this.DB.Suppliers
                            .Select(Q => new SupplierListViewModel
                            {
                                SupplierEmail = Q.SupplierEmail,
                                SupplierName = Q.SupplierName,
                                SupplierPhone = Q.SupplierPhone,
                                SupplierId = Q.SupplierId
                            }).ToListAsync();

            if (!string.IsNullOrEmpty(filter.SupplierName))
            {
                data = data.Where(Q => Q.SupplierName.ToLower().Contains(filter.SupplierName.ToLower())).ToList();
            }

            dataSupplier.TotalData = data.Count();
            
            if (filter.PageIndex != 0 && filter.ItemPerPage != 0)
            {
                data = data.Skip((filter.PageIndex - 1) * filter.ItemPerPage).Take(filter.ItemPerPage).ToList();
            }

            dataSupplier.Suppliers = data;

            return dataSupplier;
        }
    }
}
