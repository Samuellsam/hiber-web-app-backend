﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiber.Entities.Enums;
using TokoBangunanHiberWebApp.Model;
using TokoBangunanHiberWebApp.Model.Delivery;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class DeliveryService: IDeliveryService
    {
        public DeliveryService(HiberDBContext db)
        {
            this.DB = db;
        }
        private readonly HiberDBContext DB;

        public async Task<DeliveryPaginationModel> GetAllDelivery(FilterDeliveryListModel filter)
        {
            var dataDeliveries = new DeliveryPaginationModel();

            var data = await (
                        from d in DB.Deliveries
                        join u in DB.Users on d.UserId equals u.UserId
                        join t in DB.Transactions on d.TransactionId equals t.TransactionId
                        select new DeliveryViewModel
                        {
                            DeliveryId = d.DeliveryId,
                            BuyerName = t.BuyerName,
                            DeliveryStatusId = d.DeliveryStatusId,
                            DriverName = u.Username,
                            CreatedAt = d.CreatedAt,
                            DriverId = u.UserId
                        }).OrderByDescending(Q => Q.CreatedAt).AsNoTracking().ToListAsync();

            if (filter.BuyerName != null)
            {
                data = data.Where(Q => Q.BuyerName.ToLower().Contains(filter.BuyerName.ToLower())).ToList();
            }
            if (filter.DeliveryId != null)
            {
                data = data.Where(Q => Q.DeliveryId.Equals(filter.DeliveryId)).ToList();
            }
            if (filter.DeliveryStatusId != null)
            {
                data = data.Where(Q => Q.DeliveryStatusId == filter.DeliveryStatusId).ToList();
            }
            if (filter.DriverId != null)
            {
                data = data.Where(Q => Q.DriverId == filter.DriverId).ToList();
            }

            dataDeliveries.TotalData = data.Count();

            if (filter.PageIndex != 0 && filter.ItemPerPage != 0)
            {
                data = data.Skip((filter.PageIndex - 1) * filter.ItemPerPage).Take(filter.ItemPerPage).ToList();
            }

            dataDeliveries.Deliveries = data;

            return dataDeliveries;
        }

        public async Task<DeliveryFilterDataModel> GetFilterData()
        {
            var drivers = await (from d in DB.Deliveries
                           join u in DB.Users on d.UserId equals u.UserId
                           select new DropdownModel
                           {
                               label = u.Username,
                               value = u.UserId.ToString()
                           }).Distinct().ToListAsync();

            var deliveryStatuses = await DB.DeliveryStatuses
                .AsNoTracking()
                .Select(Q => new DropdownIntModel
                {
                    label = Q.DeliveryStatusName,
                    value = Q.DeliveryStatusId
                }).ToListAsync();

            return new DeliveryFilterDataModel
            {
                DeliveryStatuses = deliveryStatuses,
                Drivers = drivers
            };
        }

        public async Task<DeliveryDetailViewModel> GetDeliveryByDeliveryId(Guid deliveryId)
        {
            var delivery = await (from d in DB.Deliveries
                                  join u in DB.Users on d.UserId equals u.UserId
                                  join ds in DB.DeliveryStatuses on d.DeliveryStatusId equals ds.DeliveryStatusId
                                  join t in DB.Transactions on d.TransactionId equals t.TransactionId
                                  where d.DeliveryId == deliveryId
                                  select new DeliveryDetailViewModel
                                  {
                                      Address = d.Address,
                                      BuyerName = t.BuyerName,
                                      StatusId = d.DeliveryStatusId,
                                      DriverName = u.Username,
                                      TransactionId = t.TransactionId,
                                      TotalPrice = t.TotalPrice,
                                      ListBoughtProduct = (from td in DB.TransactionDetails
                                                           join p in DB.Products on td.ProductId equals p.ProductId
                                                           join u in DB.Units on p.UnitId equals u.UnitId
                                                           where td.TransactionId == t.TransactionId
                                                           select new DeliveryProductDetail
                                                           {
                                                               ProductName = p.ProductName,
                                                               ProductPrice = p.Price,
                                                               ProductQuantity = td.Quantity,
                                                               ProductUnitName = u.UnitName
                                                           }).ToList()
                                  }).FirstOrDefaultAsync();

            return delivery;
        }

        public async Task<int> ChangeDeliveryStatus(Guid deliveryId)
        {
            var delivery = await DB.Deliveries
                .Where(Q => Q.DeliveryId == deliveryId)
                .FirstOrDefaultAsync();

            if(delivery.DeliveryStatusId == (int)DeliveryStatusEnum.MemuatBarang)
            {
                delivery.DeliveryStatusId = (int)DeliveryStatusEnum.MenujuCustomer;
            }
            else if(delivery.DeliveryStatusId == (int)DeliveryStatusEnum.MenujuCustomer)
            {
                delivery.DeliveryStatusId = (int)DeliveryStatusEnum.BarangSampai;
            }

            await DB.SaveChangesAsync();

            return delivery.DeliveryStatusId;
        }
    }
}
