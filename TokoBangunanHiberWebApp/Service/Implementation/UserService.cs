﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiber.Entities.Enums;
using TokoBangunanHiberWebApp.Model.Auth;
using TokoBangunanHiberWebApp.Model.User;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class UserService : IUserService
    {
        public UserService(HiberDBContext db, IBCryptHasher hasher)
        {
            this.DB = db;
            this.Hasher = hasher;
        }
        private readonly HiberDBContext DB;
        private readonly IBCryptHasher Hasher;

        public async Task<UserSessionModel> GetUserSessionByUserId(Guid userId)
        {
            if(userId == Guid.Empty)
            {
                return null;
            }

            var session = await (
                        from u in DB.Users
                        join r in DB.Roles on u.RoleId equals r.RoleId
                        where u.UserId == userId
                        select new UserSessionModel
                        {
                            UserId = u.UserId,
                            Username = u.Username,
                            RoleId = r.RoleId,
                            RoleName = r.RoleName
                        }).AsNoTracking().FirstOrDefaultAsync();

            return session;
        }

        public async Task<bool> CreateUser(UserCreateModel model)
        {

            var newUser = new User
            {
                UserId = Guid.NewGuid(),
                Username = model.Username,
                Password = Hasher.HashString(model.Password),
                Name = model.Name,
                Email = model.Email,
                RoleId = model.IsAdmin ? (int)RoleEnum.Admin : (int)RoleEnum.Employee
            };

            DB.Users.Add(newUser);
            await DB.SaveChangesAsync();

            return true;
        }

        public async Task<List<UserViewModel>> GetAllDriver()
        {
            var drivers = await DB.Users
                .AsNoTracking()
                .Where(Q => Q.RoleId == (int)RoleEnum.Employee)
                .Select(Q => new UserViewModel
                {
                    Name = Q.Name,
                    UserId = Q.UserId,
                    Email = Q.Email,
                    RoleId = Q.RoleId,
                    Username = Q.Username
                }).ToListAsync();

            return drivers;
        }

        public async Task<UserPaginationModel> GetAllUser(FilterUserListModel filter)
        {
            var dataUsers = new UserPaginationModel();

            var data = await DB.Users.AsNoTracking()
                .Where(Q => Q.UserId.Equals(filter.UserId) == false)
                .Select(Q => new UserViewModel
                {
                    Email = Q.Email,
                    Name = Q.Name,
                    RoleId = Q.RoleId,
                    UserId = Q.UserId,
                    Username = Q.Username
                }).OrderBy(Q => Q.Username).AsNoTracking().ToListAsync();

            if (filter.Name != null)
            {
                data = data.Where(Q => Q.Name.ToLower().Contains(filter.Name.ToLower())).ToList();
            }
            if (filter.Username != null)
            {
                data = data.Where(Q => Q.Username.ToLower().Contains(filter.Username.ToLower())).ToList();
            }
            if (filter.Email != null)
            {
                data = data.Where(Q => Q.Email.ToLower().Contains(filter.Email.ToLower())).ToList();
            }
            if (filter.RoleId != null)
            {
                data = data.Where(Q => Q.RoleId == filter.RoleId).ToList();
            }

            dataUsers.TotalData = data.Count();

            if (filter.PageIndex != 0 && filter.ItemPerPage != 0)
            {
                data = data.Skip((filter.PageIndex - 1) * filter.ItemPerPage).Take(filter.ItemPerPage).ToList();
            }

            dataUsers.Users = data;

            return dataUsers;
        }

        public async Task<bool> IsUserHaveUnfinishedDelivery(Guid userId)
        {
            var delivery = await DB.Deliveries
                .Where(Q => Q.UserId.Equals(userId) && Q.DeliveryStatusId != (int)DeliveryStatusEnum.BarangSampai)
                .FirstOrDefaultAsync();

            if(delivery == null)
            {
                return false;
            }

            return true;
        }

        public async Task<UserUpdateResponseModel> UpdateUser(UserUpdateModel form, Guid userId)
        {
            var updatedUser = await DB.Users
                .Where(Q => Q.UserId.Equals(form.UpdatedUserId))
                .FirstOrDefaultAsync();

            var user = await DB.Users
                .Where(Q => Q.UserId.Equals(userId))
                .FirstOrDefaultAsync();

            if (user == null)
            {
                return new UserUpdateResponseModel
                {
                    IsSuccess = false,
                    ErrorMsg = "User tidak ditemukan"
                };
            }

            if (updatedUser == null)
            {
                return new UserUpdateResponseModel
                {
                    IsSuccess = false,
                    ErrorMsg = "User yang akan diedit datanya tidak ditemukan"
                };
            }

            if(updatedUser.RoleId == (int)RoleEnum.Employee && form.IsAdmin == true)
            {
                var isUserHaveDeliveryActive = await IsUserHaveUnfinishedDelivery(form.UpdatedUserId);

                if (isUserHaveDeliveryActive == true)
                {
                    return new UserUpdateResponseModel
                    {
                        IsSuccess = false,
                        ErrorMsg = "User yang akan diedit masih memiliki delivery yang aktif"
                    };
                }
            }

            if (Hasher.Verify(form.Password, user.Password) == false)
            {
                return new UserUpdateResponseModel
                {
                    IsSuccess = false,
                    ErrorMsg = "Password tidak sesuai dengan password anda saat ini"
                };
            }

            updatedUser.Username = form.Username;
            updatedUser.Name = form.Name;
            updatedUser.RoleId = form.IsAdmin ? (int)RoleEnum.Admin : (int)RoleEnum.Employee;
            updatedUser.Email = form.Email;

            await DB.SaveChangesAsync();

            return new UserUpdateResponseModel
            {
                IsSuccess = true,
                ErrorMsg = "Berhasil mengubah data user"
            };
        }

        public async Task<UserViewModel> GetUser(Guid userId) {
            var user = await DB.Users
                .Where(Q => Q.UserId.Equals(userId))
                .Select(Q => new UserViewModel
                {
                    Email = Q.Email,
                    Name = Q.Name,
                    RoleId = Q.RoleId,
                    UserId = Q.UserId,
                    Username = Q.Username
                })
                .FirstOrDefaultAsync();

            return user;
        }

        public async Task<bool?> ChangePassword(UserChangePasswordModel form, Guid userid)
        {
            var user = await DB.Users
                .Where(Q => Q.UserId.Equals(userid))
                .FirstOrDefaultAsync();

            if(user == null)
            {
                return null;
            }
            else if(Hasher.Verify(form.Password, user.Password) == false)
            {
                return false;
            }
            else if(form.NewPassword != form.ConfirmNewPassword)
            {
                return false;
            }

            user.Password = Hasher.HashString(form.NewPassword);
            await DB.SaveChangesAsync();

            return true;
        }
    }
}
