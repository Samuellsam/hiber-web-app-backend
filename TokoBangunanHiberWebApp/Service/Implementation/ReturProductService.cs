﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model.PaymentHistory;
using TokoBangunanHiberWebApp.Model.ReturProduct;
using TokoBangunanHiberWebApp.Model.Transaction;
using TokoBangunanHiberWebApp.Service.Interface;
using TokoBangunanHiber.Entities.Enums;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class ReturProductService : IReturProductService
    {
        public ReturProductService(HiberDBContext db)
        {
            this.DB = db;
        }
        private readonly HiberDBContext DB;

        public async Task<Guid> ManageReturDelivery(ReturProductCreateModel form)
        {
            if(form.IsCreateDelivery)
            {
                var newDeliveryId = Guid.NewGuid();

                var newDelivery = new Delivery
                {
                    Address = form.Address,
                    CreatedAt = DateTime.Now,
                    DeliveryId = newDeliveryId,
                    DeliveryStatusId = (int)DeliveryStatusEnum.MemuatBarang,
                    TransactionId = form.TransactionId,
                    UserId = (Guid)form.DriverId
                };

                DB.Deliveries.Add(newDelivery);
                await DB.SaveChangesAsync();

                return newDeliveryId;
            }

            return Guid.Empty;
        }

        public async Task<bool> ReturTransaction(ReturProductCreateModel form)
        {
            var deliveryId = await this.ManageReturDelivery(form);

            var productIds = form.Products.Select(Q => Q.ProductId);

            var products = await this.DB.Products.Where(Q => productIds.Contains(Q.ProductId)).ToListAsync();

            var listRetur = new List<ReturProduct>();

            foreach (var data in form.Products)
            {
                if(data.ProductQuantity > 0)
                {
                    listRetur.Add(new ReturProduct
                    {
                        ProductId = data.ProductId,
                        Quantity = data.ProductQuantity,
                        TransactionId = form.TransactionId,
                        ReturProductId = Guid.NewGuid(),
                        CreatedAt = DateTime.Now,
                        DeliveryId = deliveryId.Equals(Guid.Empty) ? null : deliveryId
                    });

                    var product = products.Where(Q => Q.ProductId.Equals(data.ProductId)).FirstOrDefault();
                    product.Stock += data.ProductQuantity;
                }
            }

            DB.ReturProducts.AddRange(listRetur);
            await DB.SaveChangesAsync();

            return true;
        }
    }
}
