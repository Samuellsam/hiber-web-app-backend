﻿using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiber.Entities.Enums;
using TokoBangunanHiberWebApp.Model.Product;
using TokoBangunanHiberWebApp.Model.Purchase_Order;
using TokoBangunanHiberWebApp.Model.Supplier;
using TokoBangunanHiberWebApp.Model.Transaction;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class HiberInfo
    {
        public string ShortName { get; set; } = "TB. Hitung Berkat";
        public string FullName { get; set; } = "Toko Bangunan Hitung Berkat";
        public string Address { get; set; } = "Jl. Raya Karanggan No.3, Karanggan, Kec. Gn. Putri, Bogor, Jawa Barat";
        public string PostalCode { get; set; } = "16960";
        public string PhoneNumber { get; set; } = "(021) 8673558";
    }
    public class InstallmentApplicationInfo
    {
        public string Title { get; set; } = "Surat Pernyataan Pembayaran Cicilan TB. Hitung Berkat";
        public string Body { get; set; } = "Melalui surat ini saya {0}, mengajukan pembayaran cicilan untuk transaksi dengan id {1} dan dengan kesepakatan bunga pada setiap pembayaran sebesar {2}% dan batas waktu hingga tanggal {3}. Dengan detail transaksi sebagai berikut : ";
        public string Footer { get; set; } = "Demikian pernyataan pembayaran cicilan ini saya lakukan tanpa paksaan dan saya bersedia menerima konsekuensi jika tidak memenuhi ketentuan-ketentuan yang telah disepakati bersama.";
    }
    public class PurchaseOrderInfo
    {
        public string Title { get; set; } = "Purchase Order";
        public string CompanyName { get; set; } = "TB. Hitung Berkat";
        public string Address { get; set; } = "Jl. Raya Karanggan No.3";
        public string DetailAddress { get; set; } = "Bogor, Jawa Barat, 16960";
        public string PhoneNumber { get; set; } = "(021) 8673558";
    }
    public class PdfService : IPdfService
    {
        public PdfService(HiberDBContext db)
        {
            this.DB = db;
            this.HiberInfo = new HiberInfo();
            this.InstallmentApplicationInfo = new InstallmentApplicationInfo();
            this.PurchaseOrderInfo = new PurchaseOrderInfo();
        }

        private readonly HiberDBContext DB;
        public HiberInfo HiberInfo { get; }
        public InstallmentApplicationInfo InstallmentApplicationInfo { get; }
        public PurchaseOrderInfo PurchaseOrderInfo { get; }

        public async Task<(Transaction, List<TransactionDetailPrintModel>)> GetTransactionData(Guid transactionId)
        {
            var transaction = await DB.Transactions
                .AsNoTracking()
                .Where(Q => Q.TransactionId == transactionId)
                .FirstOrDefaultAsync();

            var transactionDetails = await (from d in DB.TransactionDetails
                                            join p in DB.Products on d.ProductId equals p.ProductId
                                            join u in DB.Units on p.UnitId equals u.UnitId
                                            where d.TransactionId == transactionId
                                            select new TransactionDetailPrintModel
                                            {
                                                ProductName = p.ProductName,
                                                ProductQuantity = d.Quantity,
                                                ProductTotalPrice = d.Price,
                                                ProductUnitName = u.UnitName
                                            }).AsNoTracking().ToListAsync();

            return (transaction, transactionDetails);
        }

        public async Task<byte[]> GenerateReceiptPdf(Guid transactionId)
        {
            var pdfStream = new MemoryStream();
            var pdfWriter = new PdfWriter(pdfStream);
            var pdfDocument = new PdfDocument(pdfWriter);
            var document = new Document(pdfDocument, PageSize.A6);

            var (transaction, transactionDetails) = await GetTransactionData(transactionId);

            Paragraph title = new Paragraph(HiberInfo.ShortName.ToUpper())
                .SetTextAlignment(TextAlignment.CENTER)
                .SetBold()
                .SetFontSize(10);

            Paragraph alamat = new Paragraph(HiberInfo.Address)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetMargin(0)
                .SetFontSize(5);

            Paragraph noHP = new Paragraph(@"Telp : " + HiberInfo.PhoneNumber + ", Kode Pos : " + HiberInfo.PostalCode)
                .SetTextAlignment(TextAlignment.CENTER)
                .SetMargin(0)
                .SetFontSize(5);

            LineSeparator ls = new LineSeparator(new SolidLine()).SetMarginBottom(5).SetMarginTop(5);

            Table tableHeader = new Table(2).UseAllAvailableWidth();

            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("ID Transaksi").SetFontSize(5)));
            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(transaction.TransactionId.ToString()).SetTextAlignment(TextAlignment.RIGHT).SetFontSize(5)));

            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("Tanggal transaksi").SetFontSize(5)).SetFontSize(5));
            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(transaction.CreatedAt.ToString()).SetTextAlignment(TextAlignment.RIGHT).SetFontSize(5)));

            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("Nama Pembeli").SetFontSize(5)).SetFontSize(5));
            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(transaction.BuyerName).SetTextAlignment(TextAlignment.RIGHT).SetFontSize(5)));

            Table tableDetail = new Table(10).UseAllAvailableWidth();

            foreach (var detail in transactionDetails)
            {
                tableDetail.AddCell(new Cell(1, 2).SetBorder(Border.NO_BORDER).Add(new Paragraph(detail.ProductQuantity.ToString() + " " + detail.ProductUnitName.ToString()).SetFontSize(5)));
                tableDetail.AddCell(new Cell(1, 5).SetBorder(Border.NO_BORDER).Add(new Paragraph(detail.ProductName).SetTextAlignment(TextAlignment.LEFT).SetFontSize(5)));
                tableDetail.AddCell(new Cell(1, 3).SetBorder(Border.NO_BORDER).Add(new Paragraph(FormatPrice(detail.ProductTotalPrice)).SetTextAlignment(TextAlignment.RIGHT).SetFontSize(5)));
            }

            tableDetail.AddCell(new Cell(1, 2).SetBorder(Border.NO_BORDER).Add(new Paragraph("").SetFontSize(5)));
            tableDetail.AddCell(new Cell(1, 5).SetBorder(Border.NO_BORDER).Add(new Paragraph("").SetFontSize(5)));
            tableDetail.AddCell(new Cell(1, 3).SetBorder(Border.NO_BORDER).Add(new Paragraph("").SetFontSize(5)));

            tableDetail.AddCell(new Cell(1, 2).SetBorder(Border.NO_BORDER).Add(new Paragraph("").SetFontSize(5)));
            tableDetail.AddCell(new Cell(1, 5).SetBorder(Border.NO_BORDER).Add(new Paragraph("").SetFontSize(5)));
            tableDetail.AddCell(new Cell(1, 3).SetBorder(Border.NO_BORDER).Add(new Paragraph("").SetFontSize(5)));

            tableDetail.AddCell(new Cell(1, 2).SetBorder(Border.NO_BORDER).Add(new Paragraph("").SetFontSize(5)));
            tableDetail.AddCell(new Cell(1, 5).SetBorder(Border.NO_BORDER).Add(new Paragraph("Total").SetTextAlignment(TextAlignment.LEFT).SetFontSize(5)));
            tableDetail.AddCell(new Cell(1, 3).SetBorder(Border.NO_BORDER).Add(new Paragraph(FormatPrice(transaction.TotalPrice)).SetTextAlignment(TextAlignment.RIGHT).SetFontSize(5)));

            tableDetail.AddCell(new Cell(1, 2).SetBorder(Border.NO_BORDER).Add(new Paragraph("").SetFontSize(5)));
            tableDetail.AddCell(new Cell(1, 5).SetBorder(Border.NO_BORDER).Add(new Paragraph("Payment").SetTextAlignment(TextAlignment.LEFT).SetFontSize(5)));
            tableDetail.AddCell(new Cell(1, 3).SetBorder(Border.NO_BORDER).Add(new Paragraph(transaction.PaymentTypeId == (int)PaymentTypeEnum.Kredit ? "Kredit" : "Tunai").SetTextAlignment(TextAlignment.RIGHT).SetFontSize(5)));

            Paragraph footer = new Paragraph("Thank You")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetMargin(0)
                .SetFontSize(7);

            document.Add(title);
            document.Add(alamat);
            document.Add(noHP);
            document.Add(ls);
            document.Add(tableHeader);
            document.Add(ls);
            document.Add(tableDetail);
            document.Add(ls);
            document.Add(footer);
            document.Close();

            return pdfStream.ToArray();
        }

        public async Task<byte[]> GenerateInstallmentApplicationPdf(Guid transactionId)
        {
            var pdfStream = new MemoryStream();
            var pdfWriter = new PdfWriter(pdfStream);
            var pdfDocument = new PdfDocument(pdfWriter);
            var document = new Document(pdfDocument, PageSize.A4);

            var (transaction, transactionDetails) = await GetTransactionData(transactionId);

            Paragraph title = new Paragraph(InstallmentApplicationInfo.Title.ToUpper())
                .SetTextAlignment(TextAlignment.CENTER)
                .SetBold();

            Paragraph body = new Paragraph(String.Format(InstallmentApplicationInfo.Body, transaction.BuyerName, transaction.TransactionId.ToString(), transaction.Interest, transaction.DeadlineDate?.ToString("dd/MM/yyyy")))
                .SetTextAlignment(TextAlignment.LEFT)
                .SetMargin(0);

            Paragraph newLine = new Paragraph("");

            Table tableHeader = new Table(2).UseAllAvailableWidth();

            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("ID Transaksi")));
            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(transaction.TransactionId.ToString()).SetTextAlignment(TextAlignment.RIGHT)));

            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("Tanggal transaksi")));
            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(transaction.CreatedAt.ToString()).SetTextAlignment(TextAlignment.RIGHT)));


            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("Total").SetTextAlignment(TextAlignment.LEFT)));
            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(FormatPrice(transaction.TotalPrice)).SetTextAlignment(TextAlignment.RIGHT)));

            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("Payment").SetTextAlignment(TextAlignment.LEFT)));
            tableHeader.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(transaction.PaymentTypeId == (int)PaymentTypeEnum.Kredit ? "Kredit" : "Tunai").SetTextAlignment(TextAlignment.RIGHT)));

            Paragraph footer = new Paragraph(InstallmentApplicationInfo.Footer)
                .SetTextAlignment(TextAlignment.LEFT)
                .SetMargin(0);

            var currentDate = DateTime.Now.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));

            Paragraph dateNow = new Paragraph("Bogor, " + currentDate)
                .SetTextAlignment(TextAlignment.RIGHT);

            Table tableSign = new Table(UnitValue.CreatePercentArray(new float[] { 1, 1, 1, 1, 1, 1})).SetHorizontalAlignment(HorizontalAlignment.RIGHT);

            for (int i = 0; i < 2; i++)
            {
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ")));
            }

            tableSign.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.CENTER).SetBorder(Border.NO_BORDER).Add(new Paragraph("Menyetujui")));
            tableSign.AddCell(new Cell(1, 4).SetTextAlignment(TextAlignment.CENTER).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ")));
            tableSign.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.CENTER).SetBorder(Border.NO_BORDER).Add(new Paragraph("Yang Menyatakan")));

            for (int i = 0; i < 15; i++)
            {
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
                tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
                if (i == 8)
                {
                    tableSign.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.CENTER).SetFontSize(10).SetBorder(Border.NO_BORDER).Add(new Paragraph("Materai Rp 10.000")));
                }
                else
                {
                    tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ")));
                }
            }

            tableSign.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.CENTER).SetBorder(Border.NO_BORDER).Add(new Paragraph("(..............................)")));
            tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
            tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
            tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
            tableSign.AddCell(new Cell(1, 1).SetBorder(Border.NO_BORDER).Add(new Paragraph("          ")));
            tableSign.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.CENTER).SetBorder(Border.NO_BORDER).Add(new Paragraph("(..............................)")));

            document.Add(newLine);
            document.Add(newLine);
            document.Add(title);
            document.Add(newLine);
            document.Add(newLine);
            document.Add(body);
            document.Add(newLine);
            document.Add(newLine);
            document.Add(tableHeader);
            document.Add(newLine);
            document.Add(newLine);
            document.Add(newLine);
            document.Add(footer);
            document.Add(newLine);
            document.Add(newLine);
            document.Add(dateNow);
            document.Add(tableSign);
            document.Close();

            return pdfStream.ToArray();
        }

        public PageSize GetPageSize(int pageSize)
        {
            switch (pageSize)
            {
                case (int)PageSizeEnum.A0:
                    return PageSize.A0;
                case (int)PageSizeEnum.A1:
                    return PageSize.A1;
                case (int)PageSizeEnum.A2:
                    return PageSize.A2;
                case (int)PageSizeEnum.A3:
                    return PageSize.A3;
                case (int)PageSizeEnum.A4:
                    return PageSize.A4;
                case (int)PageSizeEnum.A6:
                    return PageSize.A6;
                case (int)PageSizeEnum.A7:
                    return PageSize.A7;
                case (int)PageSizeEnum.A8:
                    return PageSize.A8;
                case (int)PageSizeEnum.A9:
                    return PageSize.A9;
                case (int)PageSizeEnum.A10:
                    return PageSize.A10;
                default:
                    break;
            }

            return null;
        }

        public byte[] PrintBarcode(PrintBarcodeFormModel form)
        {
            var pdfStream = new MemoryStream();
            var pdfWriter = new PdfWriter(pdfStream);
            var pdfDocument = new PdfDocument(pdfWriter);
            var margin = (float)2.83465;
            PageSize size = GetPageSize(form.PageSizeEnum);

            var document = new Document(pdfDocument, size);
            document.SetMargins(margin, margin, margin, margin);

            // 1 cm = 28.3465 point
            var barcodeWidth = (float)((form.BarcodeWidth + 0.4) * 28.3465);
            var totalColumn = (int)((size.GetWidth() - margin) / (barcodeWidth + (2 * 28.3465)));

            if((size.GetWidth() - margin) - (totalColumn * (barcodeWidth + (2 * 28.3465))) > barcodeWidth)
            {
                totalColumn += 1;
            }

            for (var i = 0; i < form.ListBarcodeModel.Count; i++)
            {
                Table tableBarcode = new Table(totalColumn);
                Paragraph title = new Paragraph(form.ListBarcodeModel[i].ProductName.ToUpper());
                Paragraph productName = new Paragraph(form.ListBarcodeModel[i].ProductName.ToUpper()).SetFontSize(8);

                byte[] bytes = Convert.FromBase64String(form.ListBarcodeModel[i].ProductBase64BarcodeId);
                Image image = new Image(ImageDataFactory.CreatePng(bytes)).SetWidth(barcodeWidth);

                for (var j = 0; j < form.ListBarcodeModel[i].Quantity; j++)
                {
                    Table tableBarcodeInner = new Table(1).SetMarginRight(40).SetMarginBottom(40).SetKeepTogether(true);
                    tableBarcodeInner.AddCell(new Cell(1, 1).Add(image).SetTextAlignment(TextAlignment.CENTER).SetWidth(barcodeWidth));
                    tableBarcodeInner.AddCell(new Cell(1, 1).Add(productName).SetTextAlignment(TextAlignment.CENTER).SetWidth(barcodeWidth));
                    tableBarcode.AddCell(new Cell(1, 1).Add(tableBarcodeInner).SetBorder(Border.NO_BORDER)).SetWidth(barcodeWidth);
                }

                document.Add(title);
                document.Add(tableBarcode);
            }

            document.Close();

            return pdfStream.ToArray();
        }

        public string FormatPrice(decimal price)
        {
            CultureInfo culture = new CultureInfo("id-ID");
            return Decimal.Parse(price.ToString()).ToString("C", culture);
        }

        public async Task<(PurchaseOrderViewModel, List<PurchaseOrderDetailViewModel>, SupplierDetailViewModel)> GetPurchaseOrderData(Guid purchaseOrderId)
        {
            var purchaseOrder = await (from po in DB.PurchaseOrders
                                       join s in DB.Suppliers on po.SupplierId equals s.SupplierId
                                       join pos in DB.PurchaseOrderStatuses on po.PurchaseOrderStatusId equals pos.PurchaseOrderStatusId
                                       where po.PurchaseOrderId.Equals(purchaseOrderId)
                                       select new PurchaseOrderViewModel
                                       {
                                           PurchaseOrderId = po.PurchaseOrderId,
                                           SupplierName = s.SupplierName,
                                           SubmitBy = po.SubmitBy,
                                           TotalPrice = po.TotalPrice,
                                           Status = pos.PurchaseOrderStatusName,
                                           PurchaseOrderStatusId = pos.PurchaseOrderStatusId,
                                           CreatedAt = po.CreatedAt,
                                           Note = po.Note
                                       }).FirstOrDefaultAsync();

            var purchaseOrderDetails = await (from pod in DB.PurchaseOrderDetails
                                              join p in DB.Products on pod.ProductId equals p.ProductId
                                              where pod.PurchaseOrderId.Equals(purchaseOrderId)
                                              select new PurchaseOrderDetailViewModel
                                              {
                                                  ProductId = p.ProductId,
                                                  Price = pod.Price,
                                                  ProductName = p.ProductName,
                                                  Quantity = pod.Quantity,
                                                  ReturQuantity = pod.ReturQuantity
                                              }).ToListAsync();

            var supplierDetail = await (from po in DB.PurchaseOrders
                                        join s in DB.Suppliers on po.SupplierId equals s.SupplierId
                                        where po.PurchaseOrderId.Equals(purchaseOrderId)
                                        select new SupplierDetailViewModel
                                        {
                                            SupplierName = s.SupplierName,
                                            SupplierId = s.SupplierId,
                                            SupplierEmail = s.SupplierEmail,
                                            SupplierPhone = s.SupplierPhone,
                                        }).FirstOrDefaultAsync();

            return (purchaseOrder, purchaseOrderDetails, supplierDetail);
        }

        public async Task<byte[]> GeneratePurchaseOrderFile(Guid purchaseOrderId)
        {
            var pdfStream = new MemoryStream();
            var pdfWriter = new PdfWriter(pdfStream);
            var pdfDocument = new PdfDocument(pdfWriter);
            var document = new Document(pdfDocument, PageSize.A4);
            var headerFontSize = 10;

            var (purchaseOrder, purchaseOrderDetails, supplierDetail) = await GetPurchaseOrderData(purchaseOrderId);

            Paragraph title = new Paragraph(PurchaseOrderInfo.Title.ToUpper())
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(15)
                .SetBold();

            Paragraph newLine = new Paragraph("");

            Table tableOuterHeader = new Table(UnitValue.CreatePercentArray(new float[] { 1 })).SetHorizontalAlignment(HorizontalAlignment.CENTER).UseAllAvailableWidth();
            Table tableHeader = new Table(UnitValue.CreatePercentArray(new float[] { 0.5f, 0.1f, 0.4f })).SetHorizontalAlignment(HorizontalAlignment.CENTER).UseAllAvailableWidth();
            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph(PurchaseOrderInfo.CompanyName).SetFontSize(headerFontSize)));
            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph("PO ID").SetFontSize(headerFontSize)));
            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.RIGHT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph(purchaseOrder.PurchaseOrderId.ToString()).SetFontSize(headerFontSize)));

            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph(PurchaseOrderInfo.Address).SetFontSize(headerFontSize)));
            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph("PO Date").SetFontSize(headerFontSize)));
            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.RIGHT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph(purchaseOrder.CreatedAt.ToString()).SetFontSize(headerFontSize)));
            
            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph(PurchaseOrderInfo.DetailAddress).SetFontSize(headerFontSize)));
            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph(" ").SetFontSize(headerFontSize)));
            tableHeader.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.RIGHT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph(" ").SetFontSize(headerFontSize)));

            Table tableOuterHeaderInfo = new Table(UnitValue.CreatePercentArray(new float[] { 1 })).SetHorizontalAlignment(HorizontalAlignment.CENTER).UseAllAvailableWidth();
            Table tableHeaderInfo = new Table(UnitValue.CreatePercentArray(new float[] { 0.5f, 0.5f })).SetHorizontalAlignment(HorizontalAlignment.CENTER).UseAllAvailableWidth();
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph("Purchase From:".ToUpper()).SetFontSize(headerFontSize)));
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph("Ship To:".ToUpper()).SetFontSize(headerFontSize)));
            
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(purchaseOrder.SupplierName).SetMarginLeft(20).SetFontSize(headerFontSize)));
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(PurchaseOrderInfo.CompanyName).SetMarginLeft(20).SetFontSize(headerFontSize)));
            
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ").SetFontSize(headerFontSize)));
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(PurchaseOrderInfo.Address).SetMarginLeft(20).SetFontSize(headerFontSize)));

            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ").SetFontSize(headerFontSize)));
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(PurchaseOrderInfo.DetailAddress).SetMarginLeft(20).SetFontSize(headerFontSize)));

            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ").SetFontSize(headerFontSize)));
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ").SetFontSize(headerFontSize)));

            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph("Contact:".ToUpper()).SetMarginLeft(20).SetFontSize(headerFontSize)));
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).SetBold().Add(new Paragraph("Contact:".ToUpper()).SetMarginLeft(20).SetFontSize(headerFontSize)));

            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph((string.IsNullOrEmpty(supplierDetail.SupplierPhone) ? "<no phone>" : supplierDetail.SupplierPhone) + " / " + (string.IsNullOrEmpty(supplierDetail.SupplierEmail) ? "<no email>" : supplierDetail.SupplierEmail)).SetMarginLeft(20).SetFontSize(headerFontSize)));
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(PurchaseOrderInfo.PhoneNumber).SetMarginLeft(20).SetFontSize(headerFontSize)));

            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ").SetFontSize(headerFontSize)));
            tableHeaderInfo.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(" ").SetFontSize(headerFontSize)));

            Table tableOuterDetail = new Table(UnitValue.CreatePercentArray(new float[] { 1 })).SetHorizontalAlignment(HorizontalAlignment.CENTER).UseAllAvailableWidth();
            Table tableDetail = new Table(UnitValue.CreatePercentArray(new float[] { 0.5f, 0.2f, 0.3f })).SetHorizontalAlignment(HorizontalAlignment.CENTER).UseAllAvailableWidth();

            tableDetail.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBold().SetBorder(Border.NO_BORDER).Add(new Paragraph("Item Description").SetFontSize(headerFontSize)));
            tableDetail.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBold().SetBorder(Border.NO_BORDER).Add(new Paragraph("Quantity").SetFontSize(headerFontSize)));
            tableDetail.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBold().SetBorder(Border.NO_BORDER).Add(new Paragraph("Price").SetFontSize(headerFontSize)));

            foreach (var p in purchaseOrderDetails)
            {
                tableDetail.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(p.ProductName).SetFontSize(headerFontSize)));
                tableDetail.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(p.Quantity.ToString()).SetFontSize(headerFontSize)));
                tableDetail.AddCell(new Cell(1, 1).SetTextAlignment(TextAlignment.LEFT).SetBorder(Border.NO_BORDER).Add(new Paragraph(FormatPrice(p.Price)).SetFontSize(headerFontSize)));
            }

            Paragraph totalPrice = new Paragraph("Order Total "+FormatPrice(purchaseOrder.TotalPrice))
                .SetTextAlignment(TextAlignment.RIGHT)
                .SetFontSize(headerFontSize)
                .SetBold();

            document.Add(newLine);
            document.Add(title);
            document.Add(newLine);
            document.Add(tableOuterHeader.AddCell(tableHeader));
            document.Add(newLine);
            document.Add(tableOuterHeaderInfo.AddCell(tableHeaderInfo));
            document.Add(newLine);
            document.Add(tableOuterDetail.AddCell(tableDetail));
            document.Add(newLine);
            document.Add(totalPrice);
            document.Close();

            return pdfStream.ToArray();
        }
    }
}
