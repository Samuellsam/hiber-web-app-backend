﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiber.Entities.Enums;
using TokoBangunanHiberWebApp.Model.Purchase_Order;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class PurchaseOrderService : IPurchaseOrderService
    {
        private readonly HiberDBContext DB;

        public PurchaseOrderService(HiberDBContext db)
        {
            this.DB = db;
        }

        public async Task<PurchaseOrderDataGridModel> GetAllPurchaseOrder(PurchaseOrderFilterModel filter)
        {

            var dataPurchaseOrder = new PurchaseOrderDataGridModel();

            var data = await (from po in DB.PurchaseOrders
                         join s in DB.Suppliers on po.SupplierId equals s.SupplierId
                         join pos in DB.PurchaseOrderStatuses on po.PurchaseOrderStatusId equals pos.PurchaseOrderStatusId
                         select new PurchaseOrderViewModel
                         {
                             PurchaseOrderId = po.PurchaseOrderId,
                             SupplierName = s.SupplierName,
                             SubmitBy = po.SubmitBy,
                             TotalPrice = po.TotalPrice,
                             Status = pos.PurchaseOrderStatusName,
                             CreatedAt = po.CreatedAt,
                             PurchaseOrderStatusId = po.PurchaseOrderStatusId
                         })
                         .OrderByDescending(Q => Q.CreatedAt)
                         .AsNoTracking().ToListAsync();

            if (!string.IsNullOrEmpty(filter.SupplierName))
            {
                data = data.Where(Q => Q.SupplierName == filter.SupplierName).ToList();
            }
            if (!string.IsNullOrEmpty(filter.SubmitBy))
            {
                data = data.Where(Q => Q.SubmitBy == filter.SubmitBy).ToList();
            }
            if (filter.StatusId != null)
            {
                data = data.Where(Q => Q.PurchaseOrderStatusId == filter.StatusId).ToList();
            }
            if (filter.DateFrom != null)
            {
                data = data.Where(Q => Q.CreatedAt >= filter.DateFrom).ToList(); ;
            }
            if (filter.DateTo != null)
            {
                data = data.Where(Q => Q.CreatedAt <= filter.DateTo).ToList(); ;
            }

            dataPurchaseOrder.TotalData = data.Count();

            if (filter.PageIndex != 0 && filter.ItemPerPage != 0)
            {
                data = data.Skip((filter.PageIndex - 1) * filter.ItemPerPage).Take(filter.ItemPerPage).ToList();
            }

            dataPurchaseOrder.PurchaseOrders = data;

            return dataPurchaseOrder;
        }

        public async Task<PurchaseOrderViewModel> GetPurchaseOrderById(Guid purchaseOrderId)
        {
            var query = (from po in DB.PurchaseOrders
                         join s in DB.Suppliers on po.SupplierId equals s.SupplierId
                         join pos in DB.PurchaseOrderStatuses on po.PurchaseOrderStatusId equals pos.PurchaseOrderStatusId
                         select new { po, s, pos })
                         .AsNoTracking();

            var purchaseOrder = await query
                .Where(Q => Q.po.PurchaseOrderId == purchaseOrderId)
                .Select(Q => new PurchaseOrderViewModel 
                { 
                    PurchaseOrderId = Q.po.PurchaseOrderId,
                    SupplierName = Q.s.SupplierName,
                    SubmitBy = Q.po.SubmitBy,
                    TotalPrice = Q.po.TotalPrice,
                    Status = Q.pos.PurchaseOrderStatusName,
                    PurchaseOrderStatusId = Q.pos.PurchaseOrderStatusId,
                    CreatedAt = Q.po.CreatedAt,
                    Note = Q.po.Note
                }).FirstOrDefaultAsync();

            return purchaseOrder;
        }

        public async Task<DetailViewModel> GetPurchaseOrderDetail(Guid purchaseOrderId)
        {
            var detail = new DetailViewModel
            {
                purchaseOrder = await GetPurchaseOrderById(purchaseOrderId),
                PurchaseOrderDetail = await GetPurchaseOrderDetailListById(purchaseOrderId)
            };
            return detail;
        }

        public async Task<List<PurchaseOrderDetailViewModel>> GetPurchaseOrderDetailListById(Guid purchaseOrderId)
        {
            var query = (from pod in DB.PurchaseOrderDetails
                         join p in DB.Products on pod.ProductId equals p.ProductId
                         select new { pod, p })
                         .AsNoTracking();

            var purchaseOrderDetails = await query
                .Where(Q => Q.pod.PurchaseOrderId == purchaseOrderId)
                .Select(Q => new PurchaseOrderDetailViewModel 
                {
                    ProductId = Q.p.ProductId,
                    ProductName = Q.p.ProductName,
                    Quantity = Q.pod.Quantity,
                    Price = Q.pod.Price,
                    ReturQuantity = Q.pod.ReturQuantity ?? 0,
                }).ToListAsync();

            return purchaseOrderDetails;
        }

        public async Task AddProductQuantity(Guid purchaseOrderId)
        {
            var details = await DB.PurchaseOrderDetails
                .Where(Q => Q.PurchaseOrderId.Equals(purchaseOrderId))
                .ToListAsync();

            var productIds = details.Select(Q => Q.ProductId).ToList();

            var products = await DB.Products
                .Where(Q => productIds.Contains(Q.ProductId))
                .Select(Q => Q).ToListAsync();

            foreach(var p in products)
            {
                var currentDetails = details.Where(Q => Q.ProductId == p.ProductId).FirstOrDefault();
                if(currentDetails.ReturQuantity != null)
                {
                    if((currentDetails.Quantity - (int)currentDetails.ReturQuantity) > 0)
                    {
                        p.Stock += (currentDetails.Quantity - (int)currentDetails.ReturQuantity);
                    }
                    else
                    {
                        p.Stock = 0;
                    }
                }
                else
                {
                    p.Stock += currentDetails.Quantity;
                }
                p.BasePrice = currentDetails.Price;
                p.UpdatedAt = DateTime.Now;
            }
        }

        public async Task<bool> UpdatePurchaseOrderStatus(int statusId, Guid purchaseOrderId)
        {
            var purchaseOrder = await DB
                .PurchaseOrders
                .Where(Q => Q.PurchaseOrderId == purchaseOrderId)
                .FirstOrDefaultAsync();

            if(purchaseOrder == null)
            {
                return false;
            }

            if(statusId == (int)PurchaseOrderStatusEnum.Selesai)
            {
                await AddProductQuantity(purchaseOrderId);
            }

            purchaseOrder.PurchaseOrderStatusId = statusId;

            if(statusId == (int)PurchaseOrderStatusEnum.Approved)
            {
                purchaseOrder.ApprovedAt = DateTime.Now;
            }

            await DB.SaveChangesAsync();

            return true;
        }

        public async Task<bool> CreatePurchaseOrder(PurchaseOrderCreateModel model)
        {
            var status = await DB
                .PurchaseOrderStatuses
                .AsNoTracking()
                .Where(Q => Q.PurchaseOrderStatusId == 1)
                .FirstOrDefaultAsync();

            var po = new PurchaseOrder
            {
                PurchaseOrderId = Guid.NewGuid(),
                TotalPrice = model.TotalPrice,
                SupplierId = model.SupplierId,
                SubmitBy = model.SubmitBy,
                PurchaseOrderStatusId = status.PurchaseOrderStatusId
            };

            DB.PurchaseOrders.Add(po);
            await DB.SaveChangesAsync();

            var isSuccess = await CreatePurchaseOrderDetail(model, po.PurchaseOrderId);

            if(isSuccess == false)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> CreatePurchaseOrderDetail(PurchaseOrderCreateModel model, Guid purchaseOrderId)
        {
            foreach(var purchaseOrderDetail in model.PurchaseOrderDetail)
            {
                var detail = new PurchaseOrderDetail
                {
                    PurchaseOrderDetailId = Guid.NewGuid(),
                    Quantity = purchaseOrderDetail.Quantity,
                    Price = purchaseOrderDetail.Price,
                    ProductId = purchaseOrderDetail.ProductId,
                    PurchaseOrderId = purchaseOrderId
                };
                DB.PurchaseOrderDetails.Add(detail);
            }

            await DB.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ReturPurchaseOrder(ReturPurchaseOrderModel model)
        {
            var purchaseOrder = await DB.PurchaseOrders
                .Where(Q => Q.PurchaseOrderId.Equals(model.PurchaseOrderId))
                .FirstOrDefaultAsync();

            var purchaseOrderDetails = await DB.PurchaseOrderDetails
                .Where(Q => Q.PurchaseOrderId.Equals(model.PurchaseOrderId))
                .ToListAsync();

            var updatedTotalPO = (decimal)0;

            foreach (var p in model.Products)
            {
                var detail = purchaseOrderDetails
                    .Where(Q => Q.ProductId.Equals(p.ProductId))
                    .FirstOrDefault();
                if (detail.ReturQuantity == null)
                {
                    detail.ReturQuantity = 0;
                }
                detail.ReturQuantity += p.ProductQuantity;
            }

            updatedTotalPO = CalculateTotalPO(purchaseOrderDetails);

            purchaseOrder.Note = model.Note;
            purchaseOrder.TotalPrice = updatedTotalPO;
            purchaseOrder.PurchaseOrderStatusId = (int)PurchaseOrderStatusEnum.PengajuanRetur;

            await DB.SaveChangesAsync();

            return true;
        }

        public decimal CalculateTotalPO(List<PurchaseOrderDetail> list)
        {
            var total = (decimal)0;

            foreach(var detail in list)
            {
                if(detail.ReturQuantity != null)
                {
                    total += (detail.Price * (detail.Quantity - (int)detail.ReturQuantity));
                }
                else
                {
                    total += (detail.Price * detail.Quantity);
                }
            }

            return total;
        }

        public async Task<bool> ReportDamagedPurchaseOrder(ReturPurchaseOrderModel model)
        {
            var purchaseOrder = await DB.PurchaseOrders
                .Where(Q => Q.PurchaseOrderId.Equals(model.PurchaseOrderId))
                .FirstOrDefaultAsync();

            var purchaseOrderDetails = await DB.PurchaseOrderDetails
                .Where(Q => Q.PurchaseOrderId.Equals(model.PurchaseOrderId))
                .ToListAsync();

            foreach(var p in model.Products)
            {
                var detail = purchaseOrderDetails
                    .Where(Q => Q.ProductId.Equals(p.ProductId))
                    .FirstOrDefault();

                detail.Quantity -= p.ProductQuantity;
            }

            purchaseOrder.TotalPrice = CalculateTotalPO(purchaseOrderDetails);

            await DB.SaveChangesAsync();

            return true;
        }
    }
}
