﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiber.Entities.Enums;
using TokoBangunanHiberWebApp.Model.Cashier;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class CashierService : ICashierService
    {
        public CashierService(HiberDBContext db)
        {
            this.DB = db;
        }
        private readonly HiberDBContext DB;

        public async Task<CashierProductViewModel> GetProductByBarcodeIdOrName(string input)
        {
            var product = await (from p in DB.Products
                           join u in DB.Units on p.UnitId equals u.UnitId
                           where p.BarcodeId.Equals(input) || p.ProductName.ToLower().Equals(input.ToLower())
                           select new CashierProductViewModel
                           {
                               ProductId = p.ProductId,
                               BarcodeId = p.BarcodeId,
                               ProductName = p.ProductName,
                               ProductUnitName = u.UnitName,
                               ProductPrice = p.Price,
                               ProductStock = p.Stock
                           }).FirstOrDefaultAsync();

            return product;
        }

        public (Guid, Transaction, List<TransactionDetail>, PaymentHistory) CreateTransaction(CashierForm form)
        {
            var newPaymentHistory = new PaymentHistory();
            var newTransactionId = Guid.NewGuid();

            var transaction = new Transaction
            {
                TransactionId = newTransactionId,
                PaymentTypeId = form.PaymentTypeId,
                BuyerName = form.BuyerName,
                CreatedAt = DateTime.Now
            };

            if(form.PaymentTypeId == (int)PaymentTypeEnum.Kredit)
            {
                transaction.DeadlineDate = (DateTime)form.DeadlineDate;
                transaction.Interest = (decimal)form.Interest;
                transaction.IsPaid = false;
                transaction.TotalPrice = form.TotalPrice + ((decimal)form.Interest / 100 * form.TotalPrice); 
            }
            else
            {
                transaction.DeadlineDate = null;
                transaction.Interest = null;
                transaction.IsPaid = true;
                transaction.TotalPrice = form.TotalPrice;

                newPaymentHistory = new PaymentHistory
                {
                    PaymentDate = DateTime.Now,
                    PaymentHistoryId = Guid.NewGuid(),
                    TransactionId = newTransactionId,
                    Payment = form.TotalPrice
                };
            }

            var listDetails = new List<TransactionDetail>();

            foreach(var product in form.ListProduct)
            {
                var newTransactionDetail = new TransactionDetail
                {
                    TransactionDetailId = Guid.NewGuid(),
                    TransactionId = newTransactionId,
                    ProductId = product.ProductId,
                    Quantity = product.ProductQuantity,
                    Price = product.ProductTotalPrice
                };
                listDetails.Add(newTransactionDetail);
            }

            return (newTransactionId, transaction, listDetails, newPaymentHistory);
        }

        public async Task<bool> ReduceProductQuantity(CashierForm form)
        {
            var productIds = form.ListProduct.Select(Q => Q.ProductId);
            var productToUpdate = await DB.Products
                .Where(Q => productIds.Contains(Q.ProductId))
                .Select(Q => Q)
                .ToListAsync();

            foreach(var product in productToUpdate)
            {
                product.Stock -= form.ListProduct.Where(Q => Q.ProductId == product.ProductId).FirstOrDefault().ProductQuantity;
            }

            return true;
        }

        public async Task<Guid> SaveCashierData(CashierForm form)
        {
            var (transactionId, transaction, details, paymentHistory) = CreateTransaction(form);
            await ReduceProductQuantity(form);

            var newDelivery = new Delivery();

            if (form.IsCreateDelivery)
            {
                newDelivery = new Delivery
                {
                    DeliveryId = Guid.NewGuid(),
                    TransactionId = transaction.TransactionId,
                    UserId = (Guid)form.DriverId,
                    DeliveryStatusId = (int)DeliveryStatusEnum.MemuatBarang,
                    Address = form.Address,
                    CreatedAt = DateTime.Now
                };
            }

            DB.Transactions.Add(transaction);
            DB.TransactionDetails.AddRange(details);

            if(form.IsCreateDelivery)
            {
                DB.Deliveries.Add(newDelivery);
            }

            if(form.PaymentTypeId == (int)PaymentTypeEnum.Tunai)
            {
                DB.PaymentHistories.Add(paymentHistory);
            }
            
            await DB.SaveChangesAsync();
            return transactionId;
        }
    }
}
