﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model;
using TokoBangunanHiberWebApp.Model.Role;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class RoleService : IRoleService
    {
        public RoleService(HiberDBContext db)
        {
            this.DB = db;
        }
        private readonly HiberDBContext DB;

        public async Task<List<DropdownIntModel>> GetAllRole()
        {
            var roles = await DB.Roles
                                .AsNoTracking()
                                .Select(Q => new DropdownIntModel
                                {
                                    value = Q.RoleId,
                                    label = Q.RoleName
                                }).ToListAsync();

            return roles;
        }
    }
}
