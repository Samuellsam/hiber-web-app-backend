﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model.Auth;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class AuthService : IAuthService
    {
        public AuthService(HiberDBContext db, IBCryptHasher hasher)
        {
            this.DB = db;
            this.Hasher = hasher;
        }

        private readonly HiberDBContext DB;
        private readonly IBCryptHasher Hasher;

        public async Task<Guid> IsVerified(LoginFormModel form)
        {
            var username = form.Username;
            var user = await DB
                .Users
                .AsNoTracking()
                .Where(Q => Q.Username.ToLower() == form.Username.ToLower())
                .FirstOrDefaultAsync();

            if (user == null)
            {
                return Guid.Empty;
            }

            var verifyResult = this.Hasher.Verify(form.Password, user.Password);

            if (verifyResult == false)
            {
                return Guid.Empty;
            }

            return user.UserId;
        }
    }
}
