﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model.Category;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Service.Implementation
{
    public class CategoryService : ICategoryService
    {
        public CategoryService(HiberDBContext db)
        {
            this.DB = db;
        }
        private readonly HiberDBContext DB;

        public async Task<bool> CreateCategory(CategoryCreateUpdateModel form)
        {

            var newCategory = new Category
            {
                CategoryName = form.CategoryName
            };

            DB.Categories.Add(newCategory);
            await DB.SaveChangesAsync();

            return true;
        }
        public async Task<List<CategoryViewModel>> GetListCategory()
        {
            var categories = await DB.Categories.AsNoTracking()
                .OrderBy(Q => Q.CategoryId)
                .Select(Q => new CategoryViewModel
                {
                    CategoryId = Q.CategoryId,
                    CategoryName = Q.CategoryName
                }).ToListAsync();

            return categories;
        }

        public async Task<CategoryPaginationModel> GetAllCategory(FilterCategoryListModel filter)
        {
            var dataCategories = new CategoryPaginationModel();

            var data = await DB.Categories.AsNoTracking()
                .OrderBy(Q => Q.CategoryId)
                .Select(Q => new CategoryViewModel
                {
                    CategoryId = Q.CategoryId,
                    CategoryName = Q.CategoryName
                }).AsNoTracking().ToListAsync();

            if (!string.IsNullOrEmpty(filter.CategoryName))
            {
                data = data.Where(Q => Q.CategoryName.ToLower().Contains(filter.CategoryName.ToLower())).ToList();
            }

            dataCategories.TotalData = data.Count;

            if (filter.PageIndex != 0 && filter.ItemPerPage != 0)
            {
                data = data.Skip((filter.PageIndex - 1) * filter.ItemPerPage).Take(filter.ItemPerPage).ToList();
            }

            dataCategories.Categories = data;

            return dataCategories;
        }

        public async Task<CategoryViewModel> GetCategory(int categoryId)
        {
            var category = await DB.Categories.AsNoTracking()
                .Where(Q => Q.CategoryId == categoryId)
                .Select(Q => new CategoryViewModel
                {
                    CategoryId = Q.CategoryId,
                    CategoryName = Q.CategoryName
                }).FirstOrDefaultAsync();

            return category;
        }

        public async Task<CategoryCreateUpdateResponseModel> UpdateCategory(CategoryCreateUpdateModel form, int categoryId)
        {
            var category = await DB.Categories
                .Where(Q => Q.CategoryId == categoryId)
                .FirstOrDefaultAsync();

            if (string.IsNullOrEmpty(form.CategoryName))
            {
                return new CategoryCreateUpdateResponseModel
                {
                    ErrorMessage = "Nama category tidak boleh kosong",
                    IsSuccess = false
                };
            }

            if (category == null)
            {
                return new CategoryCreateUpdateResponseModel
                {
                    ErrorMessage = "Category tidak terdapat pada database",
                    IsSuccess = false
                };
            }

            category.CategoryName = form.CategoryName;

            await DB.SaveChangesAsync();

            return new CategoryCreateUpdateResponseModel
            {
                ErrorMessage = null,
                IsSuccess = true
            };
        }
    }
}
