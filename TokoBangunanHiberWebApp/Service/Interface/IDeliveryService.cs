﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Delivery;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IDeliveryService
    {
        Task<DeliveryPaginationModel> GetAllDelivery(FilterDeliveryListModel filter);
        Task<DeliveryDetailViewModel> GetDeliveryByDeliveryId(Guid deliveryId);
        Task<int> ChangeDeliveryStatus(Guid deliveryId);
        Task<DeliveryFilterDataModel> GetFilterData();
    }
}
