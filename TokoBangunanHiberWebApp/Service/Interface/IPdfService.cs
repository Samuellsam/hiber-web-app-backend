﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Product;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IPdfService
    {
        Task<byte[]> GenerateReceiptPdf(Guid transactionId);
        Task<byte[]> GenerateInstallmentApplicationPdf(Guid transactionId);
        byte[] PrintBarcode(PrintBarcodeFormModel form);
        Task<byte[]> GeneratePurchaseOrderFile(Guid purchaseOrderId);
    }
}
