﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Auth;
using TokoBangunanHiberWebApp.Model.User;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IUserService
    {
        Task<UserSessionModel> GetUserSessionByUserId(Guid userId);
        Task<bool> CreateUser(UserCreateModel model);
        Task<List<UserViewModel>> GetAllDriver();
        Task<UserPaginationModel> GetAllUser(FilterUserListModel filter);
        Task<UserUpdateResponseModel> UpdateUser(UserUpdateModel form, Guid userId);
        Task<UserViewModel> GetUser(Guid userId);
        Task<bool?> ChangePassword(UserChangePasswordModel form, Guid userid);
    }
}
