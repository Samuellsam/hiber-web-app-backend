﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model;
using TokoBangunanHiberWebApp.Model.Role;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IRoleService
    {
        Task<List<DropdownIntModel>> GetAllRole();
    }
}
