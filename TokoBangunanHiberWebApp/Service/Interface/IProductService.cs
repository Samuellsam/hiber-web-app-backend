﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model;
using TokoBangunanHiberWebApp.Model.Product;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IProductService
    {
        Task<ProductPaginationModel> GetAllProduct(ProductFilterModel filter);
        Task<bool> DeleteProduct(Guid productId);
        Task<bool> UpdateProduct(ProductEditCreateModel model, Guid productId);
        Task<bool> CreateProduct(ProductEditCreateModel model);
        Task<ProductViewModel> GetDetailProduct(Guid productId, RequestFileUrlModel urlModel);
        Task<CreateEditFormData> GetCreateEditFormData();
        Task<List<ProductDropdownModel>> GetAllProductDropdown();
    }
}
