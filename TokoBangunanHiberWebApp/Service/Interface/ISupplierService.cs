﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Supplier;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface ISupplierService
    {
        Task<List<SupplierViewModel>> GetAllAsync();
        Task<SupplierPaginationModel> GetSupplierList(FilterSupplierListModel filter); 
        Task<bool> CreateSupplier(SupplierCreateEditModel model);
        Task<bool> EditSupplier(SupplierCreateEditModel model,Guid supplierId);
        Task<SupplierDetailModel> GetSupplierDetail(Guid supplierId);
        Task<bool> DeleteSupplier(Guid supplierId);

    }
}
