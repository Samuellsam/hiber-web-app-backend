﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.ReturProduct;
using TokoBangunanHiberWebApp.Model.Transaction;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IReturProductService
    {
        Task<bool> ReturTransaction(ReturProductCreateModel form);
        Task<Guid> ManageReturDelivery(ReturProductCreateModel form);
    }
}
