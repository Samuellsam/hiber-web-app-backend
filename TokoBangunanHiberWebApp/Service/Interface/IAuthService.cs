﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Auth;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IAuthService
    {
        Task<Guid> IsVerified(LoginFormModel form);
    }
}
