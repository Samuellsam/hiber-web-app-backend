﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Category;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface ICategoryService
    {
        Task<CategoryPaginationModel> GetAllCategory(FilterCategoryListModel filter);
        Task<bool> CreateCategory(CategoryCreateUpdateModel form);
        Task<CategoryCreateUpdateResponseModel> UpdateCategory(CategoryCreateUpdateModel form, int categoryId);
        Task<CategoryViewModel> GetCategory(int categoryId);
        Task<List<CategoryViewModel>> GetListCategory();
    }
}
