﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.ReturProduct;
using TokoBangunanHiberWebApp.Model.Transaction;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface ITransactionService
    {
        Task<TransactionPaginationModel> GetAllTransaction(TransactionFilterModel filter);
        Task<TransactionDetailViewModel> GetTransactionByTransactionId(Guid transactionId);
        Task<bool> PayCreditTransaction(Guid transactionId, TransactionPayModel form);
    }
}
