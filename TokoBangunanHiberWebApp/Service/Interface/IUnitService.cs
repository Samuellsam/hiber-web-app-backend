﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Category;
using TokoBangunanHiberWebApp.Model.Unit;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IUnitService
    {
        Task<UnitPaginationModel> GetAllUnit(FilterUnitListModel filter);
        Task<bool> CreateUnit(UnitCreateUpdateModel form);
        Task<UnitCreateUpdateResponseModel> UpdateUnit(UnitCreateUpdateModel form, int unitId);
        Task<UnitViewModel> GetUnit(int unitId);
        Task<List<CategoryViewModel>> GetRemainingCategory();
    }
}
