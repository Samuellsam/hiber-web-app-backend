﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Purchase_Order;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IPurchaseOrderService
    {
        Task<PurchaseOrderDataGridModel> GetAllPurchaseOrder(PurchaseOrderFilterModel filter);
        Task<DetailViewModel> GetPurchaseOrderDetail(Guid purchaseOrderDetailId);
        Task<PurchaseOrderViewModel> GetPurchaseOrderById(Guid purchaseOrderId);
        Task<List<PurchaseOrderDetailViewModel>> GetPurchaseOrderDetailListById(Guid purchaseOrderId);
        Task<bool> UpdatePurchaseOrderStatus(int statusId, Guid purchaseOrderId);
        Task<bool> CreatePurchaseOrder(PurchaseOrderCreateModel model);
        Task<bool> CreatePurchaseOrderDetail(PurchaseOrderCreateModel model, Guid purchaseOrderId);
        Task<bool> ReportDamagedPurchaseOrder(ReturPurchaseOrderModel model);
        Task<bool> ReturPurchaseOrder(ReturPurchaseOrderModel model);
    }
}
