﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.PaymentType;

namespace TokoBangunanHiberWebApp.Service.Interface
{
    public interface IPaymentTypeService
    {
        Task<List<PaymentTypeViewModel>> GetAllPaymentType();
    }
}
