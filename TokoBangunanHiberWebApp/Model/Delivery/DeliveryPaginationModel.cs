﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Delivery
{
    public class DeliveryPaginationModel
    {
        public List<DeliveryViewModel> Deliveries { get; set; }
        public int TotalData { set; get; }
    }
}
