﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Delivery
{
    public class DeliveryFilterDataModel
    {
        public List<DropdownModel> Drivers { get; set; }
        public List<DropdownIntModel> DeliveryStatuses { get; set; }
    }
}
