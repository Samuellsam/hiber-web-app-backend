﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Delivery
{
    public class FilterDeliveryListModel
    {
        public Guid? DeliveryId { get; set; }
        public string BuyerName { get; set; }
        public Guid? DriverId { get; set; }
        public int? DeliveryStatusId { get; set; }
        public int PageIndex { set; get; }
        public int ItemPerPage { set; get; }
    }
}
