﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model
{
    public class DropdownModel
    {
        public string label { set; get; }
        public string value { set; get; }
    }

    public class DropdownIntModel
    {
        public string label { set; get; }
        public int? value { set; get; }
    }
}
