﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Dashboard
{
    public class DashboardAdminViewModel
    {
        public List<TopUnpaidTransaction> UnpaidTransactions { get; set; }
        public List<TopOutOfStockProduct> OutOfStockProducts { get; set; }
        public List<TopNeedToBeDelivered> NeedToBeDeliveredDeliveries { get; set; }
        public TotalFinancial TotalFinancial { get; set; }
    }

    public class TopUnpaidTransaction
    {
        public Guid TransactionId { get; set; }
        public string BuyerName { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalUnpaid { get; set; }
        public decimal Interest { get; set; }
        public string DeadlineDate { get; set; }
    }

    public class TopOutOfStockProduct
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public int ProductStock { get; set; }
        public decimal ProductModal { get; set; }
        public string ProductLastUpdate { get; set; }
    }

    public class TopNeedToBeDelivered
    {
        public Guid DeliveryId { get; set; }
        public string BuyerName { get; set; }
        public string DriverName { get; set; }
        public int DeliveryStatusId { get; set; }
        public string Address { get; set; }
        public string OrderDate { get; set; }
    }

    public class TotalFinancial
    {
        public decimal TotalRevenueThisMonth { get; set; }
        public decimal TotalRevenue { get; set; }
        public decimal TotalRevenueToday { get; set; }
    }
}
