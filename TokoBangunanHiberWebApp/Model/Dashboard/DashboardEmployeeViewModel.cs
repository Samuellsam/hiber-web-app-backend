﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Dashboard
{
    public class DashboardEmployeeViewModel
    {
        public List<TopOutOfStockProduct> OutOfStockProducts { get; set; }
        public List<TopNeedToBeDelivered> NeedToBeDeliveredDeliveries { get; set; }
    }
}
