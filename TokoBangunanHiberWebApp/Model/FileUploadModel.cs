﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model
{
    public class FileUploadModel
    {
        public Guid FileId { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }

    }
}
