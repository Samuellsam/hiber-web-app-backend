﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Auth
{
    public class UserSessionModel
    {
        public string Username { get; set; }
        public Guid UserId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
