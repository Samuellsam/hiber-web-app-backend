﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Supplier
{
    public class FilterSupplierListModel
    {
        public string SupplierName { set; get; }
        public int PageIndex { set; get; }
        public int ItemPerPage { set; get; }
    }
}
