﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Supplier
{
    public class SupplierCreateEditModel
    {
        public string SupplierName { set; get; }
        public string SupplierEmail { set; get; }
        public string SupplierPhone { set; get; }
    }
}
