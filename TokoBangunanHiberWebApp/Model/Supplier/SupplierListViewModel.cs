﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Supplier
{
    public class SupplierListViewModel
    {
        public Guid SupplierId { set; get; }
        public string SupplierName { set; get; }
        public string SupplierEmail { set; get; }
        public string SupplierPhone { set; get; }
        
    }
    public class SupplierPaginationModel
    {
        public int TotalData { set; get; }
        public List<SupplierListViewModel> Suppliers { set; get; }
    }
}
