﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Supplier
{
    public class SupplierViewModel
    {
        [Required]
        public Guid SupplierId { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
    }
    public class SupplierDetailViewModel
    {
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string SupplierEmail { get; set; }
        public string SupplierPhone { get; set; }
    }
}
