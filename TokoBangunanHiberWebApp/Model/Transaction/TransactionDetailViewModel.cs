﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.PaymentHistory;
using TokoBangunanHiberWebApp.Model.ReturProduct;

namespace TokoBangunanHiberWebApp.Model.Transaction
{
    public class TransactionDetailViewModel
    {
        public Guid? DeliveryId { get; set; }
        public string BuyerName { get; set; }
        public string PaymentTypeName { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal? Interest { get; set; }
        public bool IsPaid { get; set; }
        public int? DeliveryStatusId { get; set; }
        public int? ReturDeliveryStatusId { get; set; }
        public int PaymentTypeId { get; set; }
        public decimal TotalPayment { get; set; }
        public string TransactionDate { set; get; }
        public List<TransactionProductDetail> ListBoughtProduct { get; set; }
        public List<PaymentHistoryViewModel> ListPaymentHistory { get; set; }
        public List<ReturProductViewModel> ListReturHistory { get; set; }
    }

    public class TransactionProductDetail
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
