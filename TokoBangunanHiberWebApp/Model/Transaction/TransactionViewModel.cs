﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Transaction
{
    public class TransactionViewModel
    {
        public Guid TransactionId { get; set; }
        public string BuyerName { get; set; }
        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
        public decimal TotalPrice { get; set; }
        public bool IsPaid { get; set; }
        public bool IsInReturProcess { get; set; }
        public DateTime CreatedAt { get; set; }
    }

}
