﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Transaction
{
    public class TransactionPayModel
    {
        [Required]
        public decimal Payment { get; set; }
        [Required]
        public bool IsLastPayment { get; set; }
    }
}
