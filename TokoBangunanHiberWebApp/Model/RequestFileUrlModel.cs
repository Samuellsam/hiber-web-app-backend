﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model
{
    public class RequestFileUrlModel
    {
        public string Scheme { set; get; }
        public HostString Host { set; get; }
        public string PathBase { set; get; }
    }
}
