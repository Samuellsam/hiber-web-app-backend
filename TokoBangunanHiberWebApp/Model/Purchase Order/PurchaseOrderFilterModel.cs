﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Purchase_Order
{
    public class PurchaseOrderFilterModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? StatusId { get; set; }
        public string SupplierName { get; set; }
        public string SubmitBy { get; set; }
        public int PageIndex { set; get; }
        public int ItemPerPage { set; get; }

    }
}
