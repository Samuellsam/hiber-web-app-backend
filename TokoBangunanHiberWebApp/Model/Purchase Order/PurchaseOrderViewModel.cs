﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Purchase_Order
{
    public class PurchaseOrderViewModel
    {
        public Guid PurchaseOrderId { get; set; }
        public string SupplierName { get; set; }
        public string SubmitBy { get; set; }
        public  decimal TotalPrice { get; set; }
        public string Status { get; set; }
        public int PurchaseOrderStatusId { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Note { get; set; }

    }
}
