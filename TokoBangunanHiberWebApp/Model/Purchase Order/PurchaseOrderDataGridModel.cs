﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Purchase_Order
{
    public class PurchaseOrderDataGridModel
    {
        public List<PurchaseOrderViewModel> PurchaseOrders { get; set; }
        public int TotalData { get; set; }
    }
}
