﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Purchase_Order
{
    public class ReturPurchaseOrderDetail
    {
        public Guid ProductId { get; set; }
        public int ProductQuantity { get; set; }
    }

    public class ReturPurchaseOrderModel
    {
        public Guid PurchaseOrderId { get; set; }
        public string Note { get; set; }
        public List<ReturPurchaseOrderDetail> Products { get; set; }
    }
}
