﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Purchase_Order
{
    public class PurchaseOrderDetailViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public int? ReturQuantity { get; set; }
        public decimal Price { get; set; }
    }
}
