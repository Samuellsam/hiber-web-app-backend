﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Purchase_Order
{
    public class PurchaseOrderCreateModel
    {
        public decimal TotalPrice { get; set; }
        public Guid SupplierId { get; set; }
        public string SubmitBy { get; set; }
        public List<PurchaseDetailCreateModel> PurchaseOrderDetail { get; set; }
    }
}
