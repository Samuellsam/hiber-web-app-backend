﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Product
{
    public class ProductDropdownModel
    {
        public Guid ProductID { set; get; }
        public string ProductName { set; get; }
    }
}
