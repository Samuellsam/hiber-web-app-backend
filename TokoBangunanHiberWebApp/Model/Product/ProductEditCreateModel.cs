﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Product
{
    public class ProductEditCreateModel
    {
        public string ProductName { set; get; }
        public int ProductStock { set; get; }
        public int MinimumStockLimit { set; get; }
        public decimal ProductPrice { set; get; }
        public decimal ProductBasePrice { set; get; }
        public int ProductCategoryId { set; get; }
        public Guid SupplierId { set; get; }
        public string ImageName { set; get; }
        [NotMapped]
        public IFormFile ImageFile { set; get; }
        
    }
}
