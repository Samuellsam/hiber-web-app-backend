﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Product
{
    public class ProductFilterModel
    {
        public string ProductName { set; get; }
        public string ProductCategory { set; get; }
        public int PageIndex { set; get; }
        public int ItemPerPage { set; get; }
    }
}
