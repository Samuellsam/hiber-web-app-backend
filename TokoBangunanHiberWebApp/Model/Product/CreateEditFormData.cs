﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Product
{
    public class CreateEditFormData
    {
        public List<DropdownModel> SupplierList { set; get; }
        public List<DropdownModel> CategoriesList { set; get; }
        public string ImageSrc { set; get; }
    }
}
