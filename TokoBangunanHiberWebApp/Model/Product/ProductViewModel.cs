﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Product
{
    public class ProductViewModel
    {
        public Guid ProductID { set; get; }
        public string ProductName { set; get; }
        public decimal ProductPrice { set; get; }
        public decimal ProductBasePrice { set; get; }
        public int ProductStock { set; get; }
        public int MinimumStockLimit { set; get; }
        public DateTimeOffset LastUpdated { set; get; }
        public int ProductCategoryId { set; get; }
        public string BarcodeId { set; get; }
        public string CategoryName { set; get; }
        public Guid SupplierId { set; get; }
        public string SupplierName { set; get; }
        public string UnitName { set; get; }
        public string ImageName { set; get; }
        [NotMapped]
        public IFormFile ImageFile { set; get; }
        public string ImageSrc { set; get; }

    }

    public class ProductPaginationModel
    {
        public int TotalData { set; get; }
        public List<ProductViewModel> Products { set; get; }
    }
}
