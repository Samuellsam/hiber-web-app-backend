﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.ReturProduct
{
    public class ReturProductViewModel
    {
        public Guid TransactionId { get; set; }
        public Guid? DeliveryId { get; set; }
        public int DeliveryStatusId { get; set; }
        public string ProductName { get; set; }
        public int ProductQuantity { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
