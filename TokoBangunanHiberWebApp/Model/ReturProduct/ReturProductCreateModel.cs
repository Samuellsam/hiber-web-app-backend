﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.ReturProduct
{
    public class ReturProductDetail
    {
        public Guid ProductId { get; set; }
        public int ProductQuantity { get; set; }
    }

    public class ReturProductCreateModel
    {
        public Guid TransactionId { get; set; }
        public bool IsCreateDelivery { get; set; }
        public List<ReturProductDetail> Products { get; set; }
        public string Address { get; set; }
        public Guid? DriverId { get; set; }
    }
}
