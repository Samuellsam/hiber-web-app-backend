﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.PaymentHistory
{
    public class PaymentHistoryViewModel
    {
        public DateTime PaymentDate { get; set; }
        public decimal TotalPayment { get; set; }
    }
}
