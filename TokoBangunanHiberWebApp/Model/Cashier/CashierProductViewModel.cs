﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Cashier
{
    public class CashierProductCreateModel
    {
        public Guid ProductId { get; set; }
        public int ProductQuantity { get; set; }
        public decimal ProductTotalPrice { get; set; }
    }

    public class CashierProductViewModel
    {
        public Guid ProductId { get; set; }
        public string BarcodeId { get; set; }
        public string ProductName { get; set; }
        public int ProductStock { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProductUnitName { get; set; }
    }

    public class CashierForm
    {
        public List<CashierProductCreateModel> ListProduct{ get; set; }
        public bool IsCreateDelivery { get; set; }
        public string BuyerName { get; set; }
        public string Address { get; set; }
        public int PaymentTypeId { get; set; }
        public Guid? DriverId { get; set; }
        public DateTime? DeadlineDate { get; set; }
        public decimal? Interest { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
