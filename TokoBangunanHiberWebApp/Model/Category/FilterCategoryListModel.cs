﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Category
{
    public class FilterCategoryListModel
    {
        public string CategoryName { get; set; }
        public int PageIndex { set; get; }
        public int ItemPerPage { set; get; }
    }
}
