﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Category
{
    public class CategoryCreateUpdateModel
    {
        public string CategoryName { get; set; }
    }

    public class CategoryCreateUpdateResponseModel
    {
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
    }
}
