﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.Category
{
    public class CategoryPaginationModel
    {
        public List<CategoryViewModel> Categories { get; set; }
        public int TotalData { get; set; }
    }
}
