﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokoBangunanHiberWebApp.Model.User
{
    public class FilterUserListModel
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public Guid UserId { get; set; }
        public int? RoleId { get; set; }
        public int PageIndex { set; get; }
        public int ItemPerPage { set; get; }
    }
}
