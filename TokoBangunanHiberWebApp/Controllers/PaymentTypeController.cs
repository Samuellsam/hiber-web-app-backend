﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.PaymentType;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentTypeController : ControllerBase
    {
        private readonly IPaymentTypeService PaymentTypeService;

        public PaymentTypeController(IPaymentTypeService paymentTypeService)
        {
            this.PaymentTypeService = paymentTypeService;
        }

        [HttpGet]
        public async Task<List<PaymentTypeViewModel>> GetAllPaymentType()
        {
            return await this.PaymentTypeService.GetAllPaymentType();
        }
    }
}
