﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model;
using TokoBangunanHiberWebApp.Model.Product;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService service;
        private readonly IPdfService pdfService;

        public ProductController(IProductService productService, IPdfService pdfService)
        {
            this.service = productService;
            this.pdfService = pdfService;
        }

        [HttpGet]
        public async Task<ActionResult<ProductPaginationModel>> GetAllProduct([FromQuery]ProductFilterModel filter)
        {
            var data = await this.service.GetAllProduct(filter);
            return Ok(data);
        }

        [HttpGet("form-data")]
        public async Task<ActionResult<CreateEditFormData>> GetFormData()
        {
            var data = await this.service.GetCreateEditFormData();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductViewModel>>GetProductDetail(Guid id)
        {
            var requestUrl = new RequestFileUrlModel
            {
                PathBase = Request.PathBase,
                Host = Request.Host,
                Scheme = Request.Scheme
            };
            var data = await this.service.GetDetailProduct(id, requestUrl);
            return Ok(data);
        }
        [HttpPost]
        public async Task<ActionResult<bool>>CreateProduct([FromForm] ProductEditCreateModel model)
        {
            var data = await this.service.CreateProduct(model);
            return Ok(data);
        }
        [HttpPut("{id}")]
        public async Task<ActionResult<bool>>EditProduct ([FromForm] ProductEditCreateModel model, Guid id)
        {
            var data = await this.service.UpdateProduct(model, id);
            return Ok(data);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>>DeleteProduct(Guid id)
        {
            var data = await this.service.DeleteProduct(id);
            return Ok(data);
        }
        [Route("ProductList")]
        [HttpGet]
        public async Task<ActionResult<List<ProductDropdownModel>>>GetProductList()
        {
            var data = await this.service.GetAllProductDropdown();
            return Ok(data);
        }
        [HttpPost("PrintBarcode")]
        public ActionResult<byte[]> PrintBarcode([FromBody] PrintBarcodeFormModel form)
        {
            var data = this.pdfService.PrintBarcode(form);
            return Ok(data);
        }
    }
}
