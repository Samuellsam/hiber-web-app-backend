﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Purchase_Order;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseOrderController : ControllerBase
    {
        private readonly IPurchaseOrderService PurchaseOrderService;

        public PurchaseOrderController(IPurchaseOrderService purchaseOrderService)
        {
            this.PurchaseOrderService = purchaseOrderService;
        }


        [Route("PurchaseOrderFilter")]
        [HttpPost]
        public async Task<ActionResult<PurchaseOrderDataGridModel>> GetAllPurchaseOrder([FromQuery] PurchaseOrderFilterModel filter)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var data = await this.PurchaseOrderService.GetAllPurchaseOrder(filter);
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DetailViewModel>> GetPurchaseOrderDetail(Guid id)
        {
            var data = await this.PurchaseOrderService.GetPurchaseOrderDetail(id);
            return Ok(data);
        }

        [HttpPost("{purchaseOrderId:Guid}")]
        public async Task<ActionResult<bool>> UpdatePurchaseOrderStatus(Guid purchaseOrderId, int statusId)
        {
            var data = await this.PurchaseOrderService.UpdatePurchaseOrderStatus(statusId, purchaseOrderId);
            return Ok(data);
        }

        [HttpPost]
        public async Task<ActionResult<bool>> CreatePurchaseOrder([FromBody] PurchaseOrderCreateModel model)
        {
            var data = await this.PurchaseOrderService.CreatePurchaseOrder(model);
            return Ok(data);
        }
         
        [HttpPost("ReportDamagedPurchaseOrder")]
        public async Task<ActionResult<bool>> ReportDamagedPurchaseOrder([FromBody] ReturPurchaseOrderModel model)
        {
            var data = await this.PurchaseOrderService.ReportDamagedPurchaseOrder(model);
            return Ok(data);
        }

        [HttpPost("ReturPurchaseOrder")]
        public async Task<ActionResult<bool>> ReturPurchaseOrder([FromBody] ReturPurchaseOrderModel model)
        {
            var data = await this.PurchaseOrderService.ReturPurchaseOrder(model);
            return Ok(data);
        }

    }
}
