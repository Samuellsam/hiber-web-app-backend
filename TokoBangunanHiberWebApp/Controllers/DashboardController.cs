﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiber.Entities.Entities;
using TokoBangunanHiberWebApp.Model.Dashboard;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        public DashboardController(IDashboardService dashboardService)
        {
            this.DashboardService = dashboardService;
        }

        private readonly IDashboardService DashboardService;

        [HttpGet("GetAdminDashboard")]
        public async Task<DashboardAdminViewModel> GetAdminDashboard()
        {
            return await this.DashboardService.GetAdminDashboard();
        }

        [HttpGet("GetEmployeeDashboard")]
        public async Task<DashboardEmployeeViewModel> GetEmployeeDashboard(Guid userId)
        {
            return await this.DashboardService.GetEmployeeDashboard(userId);
        }
    }
}
