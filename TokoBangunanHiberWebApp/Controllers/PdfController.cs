﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PdfController : ControllerBase
    {
        public PdfController(IPdfService pdfService)
        {
            this.PdfService = pdfService;
        }

        public IPdfService PdfService { get; }

        [HttpGet("GenerateReceipt/{transactionId}")]
        public async Task<ActionResult<byte[]>> GenerateReceipt(Guid transactionId)
        {
            return await PdfService.GenerateReceiptPdf(transactionId);
        }

        [HttpGet("GenerateInstallmentApplication/{transactionId}")]
        public async Task<ActionResult<byte[]>> GenerateInstallmentApplication(Guid transactionId)
        {
            return await PdfService.GenerateInstallmentApplicationPdf(transactionId);
        }

        [HttpGet("GeneratePurchaseOrderFile/{purchaseOrderId}")]
        public async Task<ActionResult<byte[]>> GeneratePurchaseOrderFile(Guid purchaseOrderId)
        {
            return await PdfService.GeneratePurchaseOrderFile(purchaseOrderId);
        }
    }
}
