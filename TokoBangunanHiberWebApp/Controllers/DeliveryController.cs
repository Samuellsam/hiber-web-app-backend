﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Delivery;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryController : ControllerBase
    {
        private readonly IDeliveryService DeliveryService;
        public DeliveryController(IDeliveryService deliveryService)
        {
            this.DeliveryService = deliveryService;
        }

        [HttpGet]
        public async Task<ActionResult<DeliveryPaginationModel>> GetAllDelivery([FromQuery] FilterDeliveryListModel filter)
        {
            return await DeliveryService.GetAllDelivery(filter);
        }

        [HttpGet("{deliveryId:Guid}")]
        public async Task<ActionResult<DeliveryDetailViewModel>> GetDeliveryByDeliveryId(Guid deliveryId)
        {
            var delivery = await DeliveryService.GetDeliveryByDeliveryId(deliveryId);
            
            if(delivery == null)
            {
                return NotFound();
            }

            return delivery;
        }

        [HttpGet("GetFilterData")]
        public async Task<ActionResult<DeliveryFilterDataModel>> GetFilterData()
        {
            return await DeliveryService.GetFilterData();
        }

        [HttpPost("{deliveryId:Guid}")]
        public async Task<ActionResult<int>> ChangeDeliveryStatus(Guid deliveryId)
        {
            var newDeliveryStatus = await DeliveryService.ChangeDeliveryStatus(deliveryId);

            return newDeliveryStatus;
        }
    }
}
