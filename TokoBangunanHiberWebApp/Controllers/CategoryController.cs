﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Category;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService CategoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this.CategoryService = categoryService;
        }

        [HttpGet("GetListCategory")]
        public async Task<ActionResult<List<CategoryViewModel>>> GetListCategory()
        {
            return await CategoryService.GetListCategory();
        }

        [HttpGet("GetAllCategory")]
        public async Task<ActionResult<CategoryPaginationModel>> GetAllCategory([FromQuery] FilterCategoryListModel filter)
        {
            return await CategoryService.GetAllCategory(filter);
        }

        [HttpGet("GetCategory/{categoryId}")]
        public async Task<ActionResult<CategoryViewModel>> GetCategory(int categoryId)
        {
            return await CategoryService.GetCategory(categoryId);
        }

        [HttpPost("UpdateCategory/{categoryId}")]
        public async Task<ActionResult<CategoryCreateUpdateResponseModel>> UpdateCategory([FromBody] CategoryCreateUpdateModel form, int categoryId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await CategoryService.UpdateCategory(form, categoryId);

            return result;
        }

        [HttpPost("CreateCategory")]
        public async Task<ActionResult<bool>> CreateCategory([FromBody] CategoryCreateUpdateModel form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await CategoryService.CreateCategory(form);
        }
    }
}
