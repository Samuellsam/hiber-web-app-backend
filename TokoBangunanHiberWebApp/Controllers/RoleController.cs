﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model;
using TokoBangunanHiberWebApp.Model.Role;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService RoleService;

        public RoleController(IRoleService roleService)
        {
            this.RoleService = roleService;
        }

        [HttpGet]
        public async Task<List<DropdownIntModel>> GetAllRole()
        {
            return await this.RoleService.GetAllRole();
        }
    }
}
