﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Cashier;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CashierController : ControllerBase
    {
        private readonly ICashierService CashierService;
        public CashierController(ICashierService cashierService)
        {
            this.CashierService = cashierService;
        }

        [HttpGet("GetProductByBarcodeIdOrName/{input}")]
        public async Task<ActionResult<CashierProductViewModel>> GetProductByBarcodeIdOrName(string input)
        {
            if(input.Trim().Equals(String.Empty))
            {
                return BadRequest("Input tidak valid");
            }

            return await this.CashierService.GetProductByBarcodeIdOrName(input);
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> SaveCashierData([FromBody] CashierForm form)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return await this.CashierService.SaveCashierData(form);
        }
    }
}
