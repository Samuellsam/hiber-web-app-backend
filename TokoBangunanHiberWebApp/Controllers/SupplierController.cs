﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Model.Supplier;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly ISupplierService SupplierService;

        public SupplierController(ISupplierService supplierService)
        {
            this.SupplierService = supplierService;
        }

        [HttpGet(Name ="GetSupplier")]
        public async Task<ActionResult<List<SupplierViewModel>>> GetAsync()
        {
            var suppliers = await this.SupplierService.GetAllAsync();

            return Ok(suppliers);
        }
        [HttpGet("list",Name = "GetSupplierList")]
        public async Task<ActionResult<SupplierPaginationModel>> GetAllAsync([FromQuery]FilterSupplierListModel filter)
        {
            var suppliers = await this.SupplierService.GetSupplierList(filter);

            return Ok(suppliers);
        }
        [HttpGet("{id}",Name ="GetSupplierDetail")]
        public async Task<ActionResult<SupplierDetailModel>> GetDetailAsync(Guid id)
        {
            var suppliers = await this.SupplierService.GetSupplierDetail(id);
            return suppliers;
        }
        [HttpPost("create",Name ="CreateSupplier")]
        public async Task<ActionResult<bool>> CreateSupplierAsync([FromBody]SupplierCreateEditModel model)
        {
            var suppliers = await this.SupplierService.CreateSupplier(model);

            return suppliers;
        }
        [HttpPost("edit/{id}", Name = "EditSupplier")]
        public async Task<ActionResult<bool>> EditSupplierAsync([FromBody] SupplierCreateEditModel model,Guid id)
        {
            var suppliers = await this.SupplierService.EditSupplier(model,id);

            return suppliers;
        }
        [HttpPost("delete/{id}", Name = "DeleteSupplier")]
        public async Task<ActionResult<bool>> DeleteSupplierAsync( Guid id)
        {
            var suppliers = await this.SupplierService.DeleteSupplier(id);

            return suppliers;
        }


    }
}
