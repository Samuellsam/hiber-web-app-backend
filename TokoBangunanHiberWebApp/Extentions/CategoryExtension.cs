﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Service.Implementation;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Extentions
{
    public static class CategoryExtension
    {
        public static IServiceCollection AddCategoryService(this IServiceCollection services)
        {
            services.AddTransient<ICategoryService, CategoryService>();
            return services;
        }
    }
}
