﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokoBangunanHiberWebApp.Service.Implementation;
using TokoBangunanHiberWebApp.Service.Interface;

namespace TokoBangunanHiberWebApp.Extentions
{
    public static class PaymentTypeExtension
    {
        public static IServiceCollection AddPaymentTypeService(this IServiceCollection services)
        {
            services.AddTransient<IPaymentTypeService, PaymentTypeService>();
            return services;
        }
    }
}
